//
//  PietaTabs.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/17/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>

#import "Utilities.h"

#import "AppDelegate.h"

@interface PietaTabs : UITabBarController

@property (strong, nonatomic) EKEventStore *eventStore;
@property (assign, nonatomic) BOOL calendarAccessAllowed;
@property (strong, nonatomic) NSString *calendarIdentifier;

- (NSArray *) getEvents;

- (BOOL) createEvent:(NSDate *) startDate endDate:(NSDate *)endDate startTime:(NSDate *)startTime endTime:(NSDate *)endTime eventName:(NSString *)eventName selectedDays:(NSMutableArray *) selectedDays reminder:(NSInteger) reminder reminderText:(NSString *)reminderText chapter:(Chapter *)chapter;
- (BOOL) updateEvent:(NSDate *) startDate endDate:(NSDate *)endDate startTime:(NSDate *)startTime endTime:(NSDate *)endTime selectedDays:(NSMutableArray *) selectedDays reminder:(NSInteger) reminder chapter:(Chapter *)chapter event:(EKEvent *)event;

- (NSDictionary *) getScheduledEvents;
- (BOOL) removeEvent:(EKEvent *)event;

@end
