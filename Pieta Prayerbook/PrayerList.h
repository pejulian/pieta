//
//  PrayerList.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/4/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <FontAwesomeKit/FAKFontAwesome.h>
#import "FAKIonIcons.h"

#import <MGSwipeTableCell/MGSwipeTableCell.h>
#import <MGSwipeTableCell/MGSwipeButton.h>

#import <TWMessageBarManager/TWMessageBarManager.h>

#import "Utilities.h"
#import "Book.h"
#import "Chapter.h"

#import "PrayerDetail.h"

@interface PrayerList : UITableViewController <MGSwipeTableCellDelegate, UISearchDisplayDelegate>

@property (strong, nonatomic) Book *book;
@property (strong, nonatomic) NSArray<Chapter> *chapters;
@property (strong, nonatomic) NSArray<Chapter> *searchResults;

@end
