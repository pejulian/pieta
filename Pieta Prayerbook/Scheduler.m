//
//  Scheduler.m
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/12/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "Scheduler.h"

@interface Scheduler ()

@end

@implementation Scheduler

- (void) awakeFromNib {
    
    CGSize iconDim = [Utilities navBarIconDimension];
    CGFloat iconSize = [Utilities navBarIconSize];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[[FAKIonIcons iosArrowLeftIconWithSize:iconSize] imageWithSize:iconDim] style:UIBarButtonItemStylePlain target:self action:@selector(popView:)];
    [self.navigationItem setLeftBarButtonItem:backButton];
    
    UIBarButtonItem *scheduleChapter = [[UIBarButtonItem alloc] initWithImage:[[FAKIonIcons iosPaperplaneOutlineIconWithSize:iconSize] imageWithSize:iconDim] style:UIBarButtonItemStylePlain target:self action:@selector(scheduleChapter:)];
    
    self.navigationItem.rightBarButtonItems = [[NSMutableArray alloc] initWithObjects:scheduleChapter, nil];
}

// Garbage collection
- (void) dealloc {
    
    self.chapter = nil;
    self.event = nil;
    
    self.startDate = nil;
    self.endDate = nil;
    
    self.endDate = nil;
    self.endTime = nil;
    
    self.reminderTime = 0;
    self.reminderTimes = nil;
    self.repeatDays = nil;
    
    self.startDatePicker = nil;
    self.endDatePicker = nil;
    self.startTimePicker = nil;
    self.endTimePicker = nil;
    self.scheduleReminderPicker = nil;
}

- (void) viewDidDisappear:(BOOL)animated {
    
    
}

- (void) setChapter:(Chapter *)chapter {
    if ( _chapter != chapter )
        _chapter = chapter;
}

- (void) setSchedule:(Chapter *)chapter event:(EKEvent *)event {
    if ( _chapter != chapter )
        _chapter = chapter;
    if ( _event != event )
        _event = event;
    self.navigationItem.title = @"Edit Prayer Schedule";
}

- (void) datePickerValueChanged:(id)sender {
    
    NSDate *date = [sender date];
    NSString *prettyTime = [Utilities prettyTime:date];

    switch ( [sender tag] ) {
            
        // Start date
        case 0:
            
            self.startDate = date;
            self.startDate = [Utilities setTimeToDate:self.startDate time:self.startTime];
            self.scheduleStartDate.text = [Utilities prettyDate:date];

            self.endDate = [Utilities setTimeToDate:date time:self.endTime];
            self.endDatePicker.date = [Utilities setTimeToDate:date time:self.endTime];
            self.endDatePicker.minimumDate = [Utilities setTimeToDate:date time:self.endTime];
            self.scheduleEndDate.text = [Utilities prettyDate:self.endDate];
            
            break;
            
        // End date
        case 1:
            
            self.endDate = date;
            self.endDate = [Utilities setTimeToDate:self.endDate time:self.endTime];
            self.scheduleEndDate.text = [Utilities prettyDate:date];
            
            break;
            
            
        // Start time
        case 2:
            
            self.startTime = date;
            self.scheduleStartTime.text = prettyTime;
            
            self.endTime = [Utilities addAmountOfTime:date amount:10 forCalendarUnit:NSCalendarUnitMinute];
            self.endTimePicker.minimumDate = [Utilities addAmountOfTime:date amount:10 forCalendarUnit:NSCalendarUnitMinute];
            self.scheduleEndTime.text = [Utilities prettyTime:[Utilities addAmountOfTime:date amount:10 forCalendarUnit:NSCalendarUnitMinute]];
            
            self.startDate = [Utilities setTimeToDate:self.startDate time:self.startTime];
            self.endDate = [Utilities setTimeToDate:self.endDate time:self.endTime];

            break;
            
        // End time
        case 3:
            self.endTime = date;
            self.scheduleEndTime.text = prettyTime;
            self.endDate = [Utilities setTimeToDate:self.endDate time:self.endTime];
            break;
            
        default:
            break;
    }
}

- (void) scheduleChapter:(id) sender {
    
    NSMutableArray *selectedDays = [NSMutableArray new];
    [self.repeatDays each:^(NSNumber *day) {
        [selectedDays addObject:@([day integerValue] + 1) ];
    }];
    
    PietaTabs *tabs = (PietaTabs *)self.tabBarController;
    
    NSString *title = [NSString stringWithFormat:@"Recite the \"%@\"", [self.chapter title]];
    NSString *reminderText = [NSString stringWithFormat:@"It will be time to recite \"%@\" in %li minutes.", [self.chapter title], self.reminderTime ];
    
    BOOL success = NO;

    if ( self.event != nil ) {
        
        [tabs removeEvent:self.event];

        success = [tabs createEvent:self.startDate endDate:self.endDate startTime:self.startTime endTime:self.endTime eventName:title selectedDays:selectedDays reminder:self.reminderTime reminderText:reminderText chapter:self.chapter];
        
        if ( !success )
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Error" description:@"Schedule could not be updated!" type:TWMessageBarMessageTypeError duration:4.0];
        else
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Success" description:@"Prayer schedule has been updated!" type:TWMessageBarMessageTypeSuccess duration:4.0];
    }
    else {

        success = [tabs createEvent:self.startDate endDate:self.endDate startTime:self.startTime endTime:self.endTime eventName:title selectedDays:selectedDays reminder:self.reminderTime reminderText:reminderText chapter:self.chapter];
        
        if ( !success )
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Error" description:@"Schedule could not be created!" type:TWMessageBarMessageTypeError duration:4.0];
        else
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Success" description:@"View this schedule in the \"My Schedule\" tab" type:TWMessageBarMessageTypeSuccess duration:4.0];
    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void) popView:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Custom title view
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width - 150, self.navigationController.navigationBar.frame.size.height)];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setTextColor:[UIColor whiteColor]];
    
    if ( [[self.chapter title] length] > 40 )
        [titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:[UIFont smallSystemFontSize]]];
    else
        [titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:[UIFont systemFontSize]]];
    
    [titleLabel setNumberOfLines:0];
    if ( self.event != nil )
        [titleLabel setText:[NSString stringWithFormat:@"Edit schedule for \"%@\"", self.chapter.title]];
    else
        [titleLabel setText:[NSString stringWithFormat:@"New prayer schedule for \"%@\"", self.chapter.title]];
    self.navigationItem.titleView = titleLabel;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    NSSortDescriptor *highestToLowest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    self.reminderTimes = [[kReminders allKeys] mutableCopy];
    [self.reminderTimes sortUsingDescriptors:[NSArray arrayWithObject:highestToLowest]];    
    
    self.repeatDays = [NSMutableArray new];
    self.scheduleDays.delegate = self;
    self.scheduleDays.dataSource = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    NSDate *startDate = nil;
    NSString *prettyStartDate = nil;
    NSString *prettyStartTime = nil;
    
    NSDate *endDate = nil;
    NSString *prettyEndDate = nil;
    NSString *prettyEndTime = nil;
    
    EKRecurrenceRule *recurrenceRule = nil;
    EKRecurrenceEnd *recurrenceEnd = nil;
    
    if ( self.event != nil ) {
        
        recurrenceRule = (EKRecurrenceRule *)[self.event.recurrenceRules firstObject];
        recurrenceEnd = [recurrenceRule recurrenceEnd];
        
        startDate = [self.event startDate];
        endDate = [self.event endDate];
    }
    else {
        
        NSDate *date = [NSDate date];
        
        startDate = date;
        endDate = date;
    }
    
    prettyStartDate = [Utilities prettyDate:startDate];
    prettyStartTime = [Utilities prettyTime:startDate];
    
    prettyEndDate = [Utilities prettyDate:endDate];
    prettyEndTime = [Utilities prettyTime:endDate];
    
    self.startDate = startDate;
    self.startDatePicker = [UIDatePicker new];
    self.startDatePicker.minimumDate = startDate;
    self.startDatePicker.tag = 0;
    self.startDatePicker.datePickerMode = UIDatePickerModeDate;
    [self.startDatePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    self.scheduleStartDate.inputView = self.startDatePicker;
    self.scheduleStartDate.text = prettyStartDate;
    self.scheduleStartDate.delegate = self;
    
    // =========================================================================
    //  End Date
    // =========================================================================

    if ( self.event != nil )
        self.endDate = [recurrenceEnd endDate];
    else
        self.endDate = endDate;
    
    self.endDatePicker = [UIDatePicker new];
    
    if ( self.event != nil )
        self.endDatePicker.minimumDate = [recurrenceEnd endDate];
    else
        self.endDatePicker.minimumDate = endDate;
    
    self.endDatePicker.tag = 1;
    self.endDatePicker.datePickerMode = UIDatePickerModeDate;
    [self.endDatePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    self.scheduleEndDate.inputView = self.endDatePicker;
    
    if ( self.event != nil )
        self.scheduleEndDate.text = [Utilities prettyDate:[recurrenceEnd endDate]];
    else
        self.scheduleEndDate.text = prettyEndDate;
    self.scheduleEndDate.delegate = self;
    
    // =========================================================================

    
    self.startTime = startDate;
    self.startTimePicker = [UIDatePicker new];
    self.startTimePicker.minimumDate = startDate;
    self.startTimePicker.tag = 2;
    self.startTimePicker.datePickerMode = UIDatePickerModeTime;
    [self.startTimePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    self.scheduleStartTime.inputView = self.startTimePicker;
    self.scheduleStartTime.text = prettyStartTime;
    self.scheduleStartTime.delegate = self;
    
    if (self.event != nil )
        self.endTime = endDate;
    else
        self.endTime = [Utilities addAmountOfTime:endDate amount:10 forCalendarUnit:NSCalendarUnitMinute];
    self.endTimePicker = [UIDatePicker new];
    self.endTimePicker.minimumDate = endDate;
    self.endTimePicker.tag = 3;
    self.endTimePicker.datePickerMode = UIDatePickerModeTime;
    [self.endTimePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    self.scheduleEndTime.inputView = self.endTimePicker;
    if ( self.event != nil )
        self.scheduleEndTime.text = prettyEndTime;
    else
        self.scheduleEndTime.text = [Utilities prettyTime:[Utilities addAmountOfTime:endDate amount:10 forCalendarUnit:NSCalendarUnitMinute]];
    self.scheduleEndTime.delegate = self;
    
    self.scheduleReminderPicker = [UIPickerView new];
    self.scheduleReminderPicker.delegate = self;
    self.scheduleReminderPicker.dataSource = self;
    self.scheduleReminderPicker.tag = 4;
    self.scheduleReminder.inputView = self.scheduleReminderPicker;
    
    if ( self.event != nil ) {
        EKAlarm *alarm = [self.event alarms].firstObject;
        self.reminderTime = [Utilities secondsToMinutes:alarm.relativeOffset ];
        self.scheduleReminder.text = [kReminders objectForKey:[NSNumber numberWithInteger:self.reminderTime]];
    }
    else {
        self.reminderTime = 0;
        self.scheduleReminder.text = [kReminders objectForKey:@0];
    }
    
    // Set selected days
    if ( self.event != nil )
        [[recurrenceRule daysOfTheWeek] each:^(EKRecurrenceDayOfWeek *item) {
            [self.repeatDays addObject:@( [item dayOfTheWeek] - 1 )];
        }];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSNumber *key = [self.reminderTimes objectAtIndex:row];
    return [kReminders objectForKey:key];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [[kReminders allValues] count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSNumber *key = [self.reminderTimes objectAtIndex:row];
    self.reminderTime = [key integerValue];
    self.scheduleReminder.text = [kReminders objectForKey:key];
}

- (void) dismissKeyboard {
    for (UIView *view in [self.view subviews]) {
        if ([view isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField *)view;
            [textField resignFirstResponder];
        }
        else if ([view isKindOfClass:[UITextView class]]) {
            UITextView *textView = (UITextView *)view;
            [textView resignFirstResponder];
        }
        else {}
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [kDaysOfWeek count];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( [self.repeatDays containsObject:@(indexPath.row)] )
        [self.repeatDays removeObject:@(indexPath.row)];
    else
        [self.repeatDays addObject:@(indexPath.row)];
    [self.scheduleDays reloadData];
}


- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    if ( [self.repeatDays containsObject:@(indexPath.row)] )
        [self.repeatDays removeObject:@(indexPath.row)];
    else
        [self.repeatDays addObject:@(indexPath.row)];
    [self.scheduleDays reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * reuseIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if ( cell == nil )
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    [cell.textLabel setText:[kDaysOfWeek objectAtIndex:[indexPath row]]];
    
    if ( [self.repeatDays containsObject:@(indexPath.row)] )
        cell.accessoryView = [MSCellAccessory accessoryWithType:FLAT_CHECKMARK color:[UIColor colorWithRed:0/255.0 green:123/255.0 blue:170/255.0 alpha:1.0]];
    else
        cell.accessoryView = nil;
    
    return cell;
}

@end
