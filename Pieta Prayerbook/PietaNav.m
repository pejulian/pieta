//
//  PietaNav.m
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/27/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "PietaNav.h"

@interface PietaNav ()

@end

@implementation PietaNav

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setNeedsStatusBarAppearanceUpdate];

    [[UINavigationBar appearance] setBarTintColor:[[Utilities colors] objectForKey:@"tab_bar_bg"]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    // Navigation Bar Item
    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    
    // Title attributes
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
}

- (BOOL) shouldAutorotate {
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
