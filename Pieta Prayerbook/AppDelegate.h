//
//  AppDelegate.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/3/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Utilities.h"

#import <MZFormSheetController/MZFormSheetController.h>

#import <MessageUI/MessageUI.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) MFMailComposeViewController *mailer;

- (void) cycleMailComposer;

@end

