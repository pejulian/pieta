//
//  Chapter.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/3/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

#import "Content.h"

@protocol Chapter @end

@interface Chapter : JSONModel

@property (assign, nonatomic) NSInteger id;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *subtitle;
@property (strong, nonatomic) NSString *author;
@property (strong, nonatomic) NSArray<Content>* contents;
@property (assign, nonatomic) BOOL informativeText;

@end
