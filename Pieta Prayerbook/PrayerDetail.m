//
//  PrayerDetail.m
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/4/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "PrayerDetail.h"

@interface PrayerDetail ()

@end

@implementation PrayerDetail

- (void) awakeFromNib {
    
    CGSize iconDim = [Utilities navBarIconDimension];
    CGFloat iconSize = [Utilities navBarIconSize];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[[FAKIonIcons iosArrowLeftIconWithSize:iconSize] imageWithSize:iconDim] style:UIBarButtonItemStylePlain target:self action:@selector(popView:)];
    [self.navigationItem setLeftBarButtonItem:backButton];
    
    UIBarButtonItem *prayerOptions = [[UIBarButtonItem alloc] initWithImage:[[FAKIonIcons iosBriefcaseOutlineIconWithSize:iconSize] imageWithSize:iconDim] style:UIBarButtonItemStylePlain target:self action:@selector(prayerOptions:)];

    self.navigationItem.rightBarButtonItems = [[NSMutableArray alloc] initWithObjects:prayerOptions, nil];
}

- (void) prayerOptions:(id)sender {
    [self performSegueWithIdentifier:@"prayerOptionsSegue" sender:nil];
}

- (void) scheduleChapter:(id)sender {
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        [self performSegueWithIdentifier:@"schedulerSegue" sender:self.chapter];
    }];
}

- (void) popView:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void) setChapter:(Chapter *)chapter {
    if ( _chapter != chapter ) {
        _chapter = chapter;
        self.contents = [self.chapter contents];
        if ( [self.contents count] )
            [self createTextView];
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Fix for tab bar which overlaps view content
    // http://stackoverflow.com/questions/19325677/tab-bar-covers-tableview-cells-in-ios7
    self.edgesForExtendedLayout = UIRectEdgeAll;
}

- (void) bookmarkChapter:(NSNotification *) notification {
    
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        if ( [Utilities isChapterInBookmarks:[self.chapter id]] ) {
            if ( [Utilities removeChapterFromBookmarks:[self.chapter id]] ) {
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Success" description:@"Prayer has been removed from favourites list" type:TWMessageBarMessageTypeSuccess duration:2.0];
            }
        }
        else {
            if ( [Utilities addChapterToBookmarks:[self.chapter id]] ) {
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Success" description:@"Prayer add to favourites list" type:TWMessageBarMessageTypeSuccess duration:2.0];
            }
        }
    }];
}

- (void) emailChapter:(NSNotification *) notification {
    
    NSDictionary *export = [self exportTextView:self.chapter];
    NSString *path = [export objectForKey:@"pdfPath"];
    NSString *name = [export objectForKey:@"pdfFileName"];
    
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSString *title = [NSString stringWithFormat:@"Recite this chapter from the Pieta Prayerbook"];

    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        
        if( [MFMailComposeViewController canSendMail] ) {
            
            [APP.mailer setMailComposeDelegate:self];
            [APP.mailer setSubject:title];
            [APP.mailer setMessageBody:[Utilities shareDescription:name] isHTML:NO];
            [APP.mailer addAttachmentData:data mimeType:@"application/pdf" fileName:name];
            
            [self presentViewController:APP.mailer animated:YES completion:^{
                NSLog(@"Presented");
            }];
        }
        else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cannot send email" message:@"Please setup an email account on your device first" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"", nil];
            [alert setTag:0];
            [alert show];
            
            [APP cycleMailComposer];
        }
    }];
}

- (void) findString:(NSNotification *) notification {
    
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {

        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Find" message:@"Type a word or phrase to search for:" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Find", nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        alert.tag = 2;
        [alert show];
        
    }];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch ( alertView.tag) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            switch (buttonIndex) {
                case 1:
                {
                    if ( [Utilities isIOS8AndAbove] ) {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];

                    }
                }
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs://"]];
                    break;
                default:
                    break;
            }
        }
            break;
        case 2:
        {
            switch (buttonIndex) {
                case 1:
                {
                    
                    NSRange searchRange = NSMakeRange(0, [_textView.text length]);
                    NSRange textRange = [_textView.text rangeOfString:[[alertView textFieldAtIndex:0] text] options:NSCaseInsensitiveSearch range:searchRange];
                    
                    if ( textRange.location == NSNotFound ) {
                        
                        [[[UIAlertView alloc] initWithTitle:@"No matches" message:@"Word or phrase not found!" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil] show];
                    }
                    else {

                        _textView.selectedRange = textRange;
                        [_textView scrollRangeToVisible:NSMakeRange(textRange.location + 150, textRange.length)];
                        
                        NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithAttributedString:_textView.attributedText];
                        [content addAttribute:NSBackgroundColorAttributeName value:[UIColor yellowColor] range:textRange];
                        _textView.attributedText = content;
                    }
                }
                    break;
                default:
                    break;
            }
        }
        default:
            break;
    }
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scheduleChapter:) name:@"scheduleChapter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bookmarkChapter:) name:@"bookmarkChapter" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(emailChapter:) name:@"emailChapter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(findString:) name:@"findString" object:nil];

    // Custom title view
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width - 150, self.navigationController.navigationBar.frame.size.height)];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setText:[self.chapter title]];
    [titleLabel setNumberOfLines:0];

    if ( [[self.chapter title] length] > 40 )
        [titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:[UIFont smallSystemFontSize]]];
    else
        [titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:[UIFont systemFontSize]]];
    
    if ( [[self.chapter title] length] > 40 )
        [titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleFootnote]];
    else
        [titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleBody]];

    self.navigationItem.titleView = titleLabel;
    
    // Prevent navigation bar & tab bar from overlapping view
    self.navigationController.navigationBar.translucent = NO;
    self.tabBarController.tabBar.translucent = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(preferredContentSizeChanged:) name:UIContentSizeCategoryDidChangeNotification object:nil];
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIContentSizeCategoryDidChangeNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"scheduleChapter" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"bookmarkChapter" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"emailChapter" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"findString" object:nil];

}

- (void) dealloc {
    
    self.chapter = nil;
    self.contents = nil;
}


# pragma Mail delegates

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    if ( error ) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"" delegate:nil cancelButtonTitle:@"" otherButtonTitles:@"", nil];
        [alert show];
        [APP cycleMailComposer];

    }
    else {
        
        switch (result)
        {
            case MFMailComposeResultCancelled:
                NSLog(@"Result: Mail sending canceled");
                break;
            case MFMailComposeResultSaved:
                NSLog(@"Result: Mail saved");
                break;
            case MFMailComposeResultSent:
                NSLog(@"Result: Mail sent");
                break;
            case MFMailComposeResultFailed:
                NSLog(@"Result: Mail sending failed");
                break;
            default:
                NSLog(@"Result: Mail not sent");
                break;
        }
        
        [self dismissViewControllerAnimated:YES completion:^{
            [APP cycleMailComposer];
        }];
    }
}


// http://stackoverflow.com/questions/5443166/how-to-convert-uiview-to-pdf-within-ios/6566696#6566696
- (NSString *)createPDFfromUIView:(UIView*)aView saveToDocumentsWithFileName:(NSString*)aFilename {
    
    // Creates a mutable data object for updating with binary data, like a byte array
    NSMutableData *pdfData = [NSMutableData data];
    
    // Points the pdf converter to the mutable data object and to the UIView to be converted
    UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil);
    UIGraphicsBeginPDFPage();
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    
    // draws rect to the view and thus this is captured by UIGraphicsBeginPDFContextToData
    
    [aView.layer renderInContext:pdfContext];
    
    // remove PDF rendering context
    UIGraphicsEndPDFContext();
    
    // Retrieves the document directories from the iOS device
    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    
    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
    NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:aFilename];
    
    // instructs the mutable data object to write its context to a file on disk
    [pdfData writeToFile:documentDirectoryFilename atomically:YES];
    
    return documentDirectoryFilename;
}

// http://stackoverflow.com/questions/3539717/getting-a-screenshot-of-a-uiscrollview-including-offscreen-parts
- (NSDictionary *) exportTextView:(Chapter *)chapter {
    
    NSString *imageFileName = [NSString stringWithFormat:@"The Pieta Prayerbook - %@.jpg", self.chapter.title ];
    NSString *pdfFileName = [NSString stringWithFormat:@"The Pieta Prayerbook - %@.pdf", self.chapter.title ];
  
    NSArray *arrayPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [arrayPaths objectAtIndex:0];
    
    NSString *imagePath = [path stringByAppendingPathComponent:imageFileName];
    NSString *pdfPath = [path stringByAppendingPathComponent:pdfFileName];

    NSLog(@"Asset path is: %@", path );

    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ( [fileManager fileExistsAtPath:imageFileName] && [fileManager fileExistsAtPath:pdfFileName ] ) {
        
        return @{
            @"imageFileName": imageFileName,
            @"imagePath": imagePath,
            @"pdfPath": pdfPath,
            @"pdfFileName": pdfFileName
        };
    }
    else {
        
        UIImage* image = nil;
        
        NSTextStorage *textStorage = [Utilities buildTextForChapter:chapter];
        NSAttributedString *attributedText = [textStorage attributedSubstringFromRange:NSMakeRange(0, [textStorage length])];
        
        UITextView *calculationView = [[UITextView alloc] init];
        [calculationView setAttributedText:attributedText];
        CGSize size = [calculationView sizeThatFits:CGSizeMake( 612, FLT_MAX)];
        
        CGPoint savedContentOffset = self.textView.contentOffset;
        CGRect savedFrame = CGRectMake(self.textView.frame.origin.x, self.textView.frame.origin.y, 612, size.height);

        UIGraphicsBeginImageContextWithOptions(CGSizeMake( 612, size.height), NO, 0.0 );
        {
            NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
            NSTextContainer *container = [[NSTextContainer alloc] initWithSize:savedFrame.size];
            container.widthTracksTextView = YES;
            container.heightTracksTextView = YES;

            [layoutManager addTextContainer:container];
            [textStorage addLayoutManager:layoutManager];
            
            UITextView *textView = [[UITextView alloc] initWithFrame:savedFrame textContainer:container];
            textView.contentOffset = CGPointZero;
            textView.frame = CGRectMake(0, 0, 612, size.height);
            
            [textView.layer renderInContext: UIGraphicsGetCurrentContext()];
            image = UIGraphicsGetImageFromCurrentImageContext();
            
            textView.contentOffset = savedContentOffset;
            textView.frame = savedFrame;
        }
        UIGraphicsEndImageContext();

        if (image != nil)
            [UIImageJPEGRepresentation(image, 1.0) writeToFile:imagePath atomically:YES];
        
        NSString *pdfFilePath = [self createPDFfromUIView:[[UIImageView alloc] initWithImage:image] saveToDocumentsWithFileName:pdfFileName];

        return @{
            @"imageFileName": imageFileName,
            @"imagePath": imagePath,
            @"pdfPath": pdfFilePath,
            @"pdfFileName": pdfFileName
        };
    }
}

- (void)preferredContentSizeChanged:(NSNotification *)notification {
    [self createTextView];
}

- (void)updateTextViewSize {
    
    // UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    // self.textView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

- (void)createTextView {

    NSTextStorage *textStorage = [Utilities buildTextForChapter:self.chapter];
    
    // =======================================
    // Text View Setup
    // =======================================
    
    CGRect newTextViewRect = self.view.bounds;
    
    // 1. Create the layout manager
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    
    // 2. Create a text container
    CGSize containerSize = CGSizeMake(newTextViewRect.size.width, newTextViewRect.size.height);
    
    NSTextContainer *container = [[NSTextContainer alloc] initWithSize:containerSize];
    container.widthTracksTextView = NO;
    container.heightTracksTextView = NO;
    
    [layoutManager addTextContainer:container];
    [textStorage addLayoutManager:layoutManager];
    
    // 4. Create a UITextView
    _textView = [[UITextView alloc] initWithFrame:
                 CGRectMake(newTextViewRect.origin.x, newTextViewRect.origin.y,  newTextViewRect.size.width, newTextViewRect.size.height - 100) textContainer:container];
    
    _textView.editable = NO;
    _textView.delegate = self;
    _textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;

    // 6. Add text view to main view
    [self.view addSubview:_textView];
    
    // Ugly hack to simulate a scroll (this solves scrolling issue with search)
    _textView.contentOffset = CGPointMake(0, 1);
    _textView.contentOffset = CGPointMake(0, 0);
}

- (void)updateViewConstraints {
    [super updateViewConstraints];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height - 20;
    
    if ( [[segue identifier] isEqualToString:@"prayerOptionsSegue"]  ) {
        
        MZFormSheetController *formSheet = ((MZFormSheetSegue *)segue).formSheetController;
        float height = ([[segue destinationViewController] cellHeight] * [[[segue destinationViewController] items] count]);
                
        formSheet.presentedFormSheetSize = CGSizeMake( self.view.frame.size.width * 0.9, height );
        formSheet.portraitTopInset = screenHeight - height;

        [[segue destinationViewController] setChapter:self.chapter];
        
        formSheet.didDismissCompletionHandler = ^(UIViewController *vc ) {
            [self.navigationItem.leftBarButtonItems[0] setEnabled:YES];
        };
        
        formSheet.willPresentCompletionHandler = ^(UIViewController *vc) {
            [self.navigationItem.leftBarButtonItems[0] setEnabled:NO];
            vc.view.autoresizingMask = vc.view.autoresizingMask | UIViewAutoresizingFlexibleWidth;
        };
    }
    else if ( [[segue identifier] isEqualToString:@"sharingOptionsSegue"] ) {
        
        MZFormSheetController *formSheet = ((MZFormSheetSegue *)segue).formSheetController;
        float height = ([[segue destinationViewController] cellHeight] * [[[segue destinationViewController] items] count]);
        
        formSheet.presentedFormSheetSize = CGSizeMake( self.view.frame.size.width * 0.9, height );
        formSheet.portraitTopInset = screenHeight - height;
        
        [[segue destinationViewController] setChapter:self.chapter];
        
        formSheet.didDismissCompletionHandler = ^(UIViewController *vc ) {
            [self.navigationItem.leftBarButtonItems[0] setEnabled:YES];
        };
        
        formSheet.willPresentCompletionHandler = ^(UIViewController *vc) {
            [self.navigationItem.leftBarButtonItems[0] setEnabled:NO];
            vc.view.autoresizingMask = vc.view.autoresizingMask | UIViewAutoresizingFlexibleWidth;
        };
    }
    else if ( [[segue identifier] isEqualToString:@"schedulerSegue"] ) {
        
        [[segue destinationViewController] setChapter:self.chapter];
        
    }
    else {}

}

@end
