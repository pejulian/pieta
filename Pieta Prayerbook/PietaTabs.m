//
//  PietaTabs.m
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/17/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "PietaTabs.h"

@interface PietaTabs ()

@end

@implementation PietaTabs

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Override point for customization after application launch.
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName:[UIColor darkGrayColor]} forState:UIControlStateSelected];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    // =======================================================================
    // Tab Bar Themes
    // =======================================================================
    [[UITabBar appearance] setBarTintColor:[[Utilities colors] objectForKey:@"tab_bar_bg"]];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setTranslucent:NO];
    [[UITabBar appearance] setOpaque:NO];

    // =======================================================================
    // Tab Bar Item Unselected
    // =======================================================================
    [[UITabBarItem appearance] setTitleTextAttributes:@{
        NSForegroundColorAttributeName:[UIColor whiteColor],
        NSFontAttributeName: [UIFont systemFontOfSize:11]
    } forState:UIControlStateNormal];
        
    // =======================================================================
    // Tab Bar Item Selected
    // =======================================================================
    [[UITabBarItem appearance] setTitleTextAttributes:@{
        NSForegroundColorAttributeName:[UIColor whiteColor],
        NSFontAttributeName: [UIFont systemFontOfSize:11]
    } forState:UIControlStateSelected];

    self.eventStore = [[EKEventStore alloc] init];
    [self.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if ( error == nil ) {
            self.calendarAccessAllowed = granted;
            [self loadPietaCalendar];
        }
        else {
            NSLog(@"%@", [error localizedDescription]);
        }
    }];
    
}

- (BOOL) shouldAutorotate {
    return NO;
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (void) dealloc {
    
    self.eventStore = nil;
    self.calendarAccessAllowed = nil;
    self.calendarIdentifier = nil;
}

- (void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
}

- (EKCalendar *) loadPietaCalendar {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults objectForKey:@"pieta_calendar_identifier"] != nil)
        self.calendarIdentifier = [userDefaults objectForKey:@"pieta_calendar_identifier"];
    else
        [self createPietaCalendar];
    
    EKCalendar *pietaCalendar = nil;
    if ( self.calendarAccessAllowed ) {
        NSArray *calendars = [self.eventStore calendarsForEntityType:EKEntityTypeEvent];
        for ( EKCalendar *calendar in calendars )
            if ( [[calendar calendarIdentifier] isEqualToString:self.calendarIdentifier] ) {
                pietaCalendar = calendar;
                break;
            }
    }

    return pietaCalendar;
}

- (void) createPietaCalendar {
    
    if ( self.calendarAccessAllowed ) {

        EKCalendar *calendar = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:self.eventStore];
        calendar.title = @"Pieta Prayerbook";
        
        EKSource *localSource = nil;
        for (EKSource *source in self.eventStore.sources)
            if (source.sourceType == EKSourceTypeCalDAV && [source.title isEqualToString:@"iCloud"]) {
                localSource = source;
                break;
            }
        
        if (localSource == nil)
            for (EKSource *source in self.eventStore.sources)
                if (source.sourceType == EKSourceTypeLocal) {
                    localSource = source;
                    break;
                }
        
        calendar.source = localSource;
        
        NSError *error = nil;
        [self.eventStore saveCalendar:calendar commit:YES error:&error];
        
        if ( error == nil ) {
            self.calendarIdentifier = calendar.calendarIdentifier;
            [[NSUserDefaults standardUserDefaults]
             setObject:calendar.calendarIdentifier forKey:@"pieta_calendar_identifier"];
        }
        else
            NSLog(@"Error creating Pieta Calendar: %@", [error localizedDescription]);
    }
}

- (NSArray *) getEvents {
    
    if ( self.calendarAccessAllowed ) {
        
        EKCalendar *pietaCalendar = [self loadPietaCalendar];
        
        if ( pietaCalendar != nil ) {
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            
            if ( pietaCalendar == nil )
                NSLog(@"nil calendar");
            
            // Create the start date components
            NSDateComponents *oneDayAgoComponents = [[NSDateComponents alloc] init];
            oneDayAgoComponents.day = -1;
            NSDate *oneDayAgo = [calendar dateByAddingComponents:oneDayAgoComponents toDate:[NSDate date] options:0];
            
            // Create the end date components
            NSDateComponents *oneYearFromNowComponents = [[NSDateComponents alloc] init];
            oneYearFromNowComponents.year = 2;
            NSDate *oneYearFromNow = [calendar dateByAddingComponents:oneYearFromNowComponents toDate:[NSDate date] options:0];
            
            // Create the predicate from the event store's instance method
            NSPredicate *predicate = [self.eventStore predicateForEventsWithStartDate:oneDayAgo endDate:oneYearFromNow calendars:@[pietaCalendar]];
            
            // Fetch all events that match the predicate
            return [self.eventStore eventsMatchingPredicate:predicate];
        }
        else {
            NSLog(@"Pieta calendar does not exist!");
            return @[];
        }
    }
    else {
        NSLog(@"Application has not been allowed to access to the device calendar");
        return @[];
    }
}


- (NSDictionary *) getScheduledEvents {
    
    if ( self.calendarAccessAllowed ) {
        
        NSArray *eventIdentifiers = [Utilities getUserSchedule];
        NSMutableDictionary *groupedEvents = [NSMutableDictionary new];
        
        [eventIdentifiers each:^(NSDictionary *lineItem ) {
            
            EKEvent *event = [self.eventStore eventWithIdentifier:[lineItem objectForKey:@"eventIdentifier"]];
            [groupedEvents setValue:@{@"event": event, @"chapter": [Utilities getChapterById:[[lineItem objectForKey:@"chapterId"] integerValue]]} forKey:[lineItem objectForKey:@"eventIdentifier"]];

        }];
        
        return groupedEvents;
    }
    else {
        NSLog(@"Application has not been allowed to access to the device calendar");
        return @{};
    }
}

- (BOOL) removeEvent:(EKEvent *)event {
    
    if ( self.calendarAccessAllowed ) {
        
        NSError *error = nil;
        NSString *eventIdentifier = event.eventIdentifier;
                
        BOOL result = [self.eventStore removeEvent:event span:EKSpanFutureEvents commit:YES error:&error];
        
        if ( error != nil )
            NSLog(@"Event could not be deleted: %@", [error localizedDescription]);
        else
            [Utilities removeChapterFromUserSchedule:eventIdentifier];
        
        return result;
    }
    else {
        NSLog(@"Application has not been allowed to access to the device calendar");
        return NO;
    }
}

- (BOOL) createEvent:(NSDate *) startDate endDate:(NSDate *)endDate startTime:(NSDate *)startTime endTime:(NSDate *)endTime eventName:(NSString *)eventName selectedDays:(NSMutableArray *) selectedDays reminder:(NSInteger) reminder reminderText:(NSString *)reminderText chapter:(Chapter *)chapter {

    
    // Create a new event object
    EKEvent *event = [EKEvent eventWithEventStore:self.eventStore];
    event.title = eventName;
    event.allDay = NO;
    event.availability = EKEventAvailabilityFree;
    event.timeZone = [NSTimeZone defaultTimeZone];
    
    // Recurring event, setup a recurrence rule
    EKRecurrenceRule *rule = nil;
    if ( selectedDays != nil && [selectedDays count] > 0 ) {
        
        NSMutableArray *daysOfWeek = [NSMutableArray new];
        [selectedDays each:^(NSNumber *selectedDay) {
            [daysOfWeek addObject:[EKRecurrenceDayOfWeek dayOfWeek:[selectedDay integerValue]]];
        }];
        
        rule = [[EKRecurrenceRule alloc]
                initRecurrenceWithFrequency:EKRecurrenceFrequencyWeekly
                interval:1
                daysOfTheWeek: daysOfWeek
                daysOfTheMonth:nil
                monthsOfTheYear:nil
                weeksOfTheYear:nil
                daysOfTheYear:nil
                setPositions:nil
                end:[EKRecurrenceEnd recurrenceEndWithEndDate:endDate]];
        
        event.recurrenceRules = @[rule];
    }
    else {
        event.recurrenceRules = nil;
    }
    
    // Calendar reminders
    if ( reminder > 0 ) {
        EKAlarm *alarm = [EKAlarm alarmWithRelativeOffset:-(reminder * 60)];
        [event addAlarm:alarm];
    }
    
    // Set the event calendar
    event.calendar = [self loadPietaCalendar];
    
    // Start dates and end dates
    event.startDate = startDate;
    event.endDate = [Utilities addAmountOfTime:startDate amount:[Utilities differenceBetweenTimes:startTime end:endTime forCalendarUnit:NSCalendarUnitMinute] forCalendarUnit:NSCalendarUnitMinute];
    
    NSError *error;
    if ([self.eventStore saveEvent:event span:EKSpanFutureEvents commit:YES error:&error]) {
        
        // In this context we will be able to get the newly created eventIdentifier
        // and save it to user preferences
        [Utilities addChapterToUserSchedule:event.eventIdentifier chapter:chapter.id];
        return YES;
    }
    else {
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
}

- (BOOL) updateEvent:(NSDate *) startDate endDate:(NSDate *)endDate startTime:(NSDate *)startTime endTime:(NSDate *)endTime selectedDays:(NSMutableArray *) selectedDays reminder:(NSInteger) reminder chapter:(Chapter *)chapter event:(EKEvent *)event {

    EKRecurrenceRule *rule = nil;
    if ( selectedDays != nil && [selectedDays count] > 0 ) {
        
        NSMutableArray *daysOfWeek = [NSMutableArray new];
        [selectedDays each:^(NSNumber *selectedDay) {
            [daysOfWeek addObject:[EKRecurrenceDayOfWeek dayOfWeek:[selectedDay integerValue]]];
        }];
        
        rule = [[EKRecurrenceRule alloc]
                initRecurrenceWithFrequency:EKRecurrenceFrequencyWeekly
                interval:1
                daysOfTheWeek: daysOfWeek
                daysOfTheMonth:nil
                monthsOfTheYear:nil
                weeksOfTheYear:nil
                daysOfTheYear:nil
                setPositions:nil
                end:[EKRecurrenceEnd recurrenceEndWithEndDate:endDate]];
        
        event.recurrenceRules = @[rule];
    }
    else {
        event.recurrenceRules = nil;
    }
    
    // Calendar reminders
    if ( reminder > 0 ) {
        EKAlarm *alarm = [EKAlarm alarmWithRelativeOffset:-(reminder * 60)];
        [event addAlarm:alarm];
    }

    // Start dates and end dates
    event.startDate = startDate;
    event.endDate = [Utilities addAmountOfTime:startDate amount:[Utilities differenceBetweenTimes:startTime end:endTime forCalendarUnit:NSCalendarUnitMinute] forCalendarUnit:NSCalendarUnitMinute];
    
    NSError *error;
    if ([self.eventStore saveEvent:event span:EKSpanFutureEvents commit:YES error:&error]) {
        
        // In this context we will be able to get the newly created eventIdentifier
        // and save it to user preferences
        [Utilities addChapterToUserSchedule:event.eventIdentifier chapter:chapter.id];
        return YES;
    }
    else {
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
