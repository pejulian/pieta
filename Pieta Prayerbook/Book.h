//
//  Book.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/3/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

#import "Chapter.h"

@protocol Book @end

@interface Book : JSONModel

@property (strong, nonatomic) NSArray<Chapter>* chapters;

@end
