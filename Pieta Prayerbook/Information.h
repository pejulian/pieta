//
//  Information.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 1/19/15.
//  Copyright (c) 2015 Julian Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <ObjectiveSugar/ObjectiveSugar.h>

#import <FontAwesomeKit/FAKFontAwesome.h>
#import "FAKIonIcons.h"

#import "Utilities.h"

@interface Information : UIViewController <UITextViewDelegate>

@property (strong, nonatomic) Book *book;
@property (strong, nonatomic) Chapter *chapter;
@property (strong, nonatomic) UITextView* textView;

@end
