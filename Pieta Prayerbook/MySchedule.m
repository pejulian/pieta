//
//  MySchedule.m
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/8/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "MySchedule.h"

@interface MySchedule ()

@end

@implementation MySchedule

- (void) awakeFromNib {

    CGFloat iconSize = [Utilities tabBarIconSize];
    FAKIcon *iconSelected = [FAKIonIcons iosStopwatchIconWithSize:iconSize];
    FAKIcon *icon = [FAKIonIcons iosStopwatchOutlineIconWithSize:iconSize];
    
    [iconSelected setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [icon setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    CGSize iconDim = [Utilities tabBarIconDimension];
    self.tabBarItem.image = [[icon imageWithSize:iconDim] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.tabBarItem.selectedImage = [[iconSelected imageWithSize:iconDim] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    self.tabBarItem.title = nil;
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width - 150, self.navigationController.navigationBar.frame.size.height)];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [titleLabel setNumberOfLines:0];
    [titleLabel setTextColor:[UIColor whiteColor]];
    
    NSDictionary *titleAttr = @{ NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:[UIFont labelFontSize]] };
    
    NSAttributedString* attributedTitle = [[NSAttributedString alloc] initWithString:@"Prayer Schedule" attributes:titleAttr];
    [titleLabel setAttributedText:attributedTitle];
    self.navigationItem.titleView = titleLabel;
    
    
    PietaTabs *tabs = (PietaTabs *)self.tabBarController;
    
    NSDictionary *schedules = [tabs getScheduledEvents];
    self.schedules = [schedules allValues];
    self.eventIdentifiers = [schedules allKeys];
    
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 0.5);
    dispatch_after(delay, dispatch_get_main_queue(), ^(void) {
        [self.navigationItem setPrompt:@"Prayers that you chosen to recite are listed here"];
    });
    
    delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 3.0);
    dispatch_after(delay, dispatch_get_main_queue(), ^(void) {
        [self.navigationItem setPrompt:nil];
    });
}

- (void) dealloc {
    
    self.eventIdentifiers = nil;
    self.schedules = nil;
    self.eventToDelete = nil;
}

- (void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    self.eventIdentifiers = nil;
    self.schedules = nil;
    self.eventToDelete = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if ( [self.schedules count] > 0 ) {
     
        self.tableView.backgroundView = nil;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        
        return 1;
    }
    else {
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        messageLabel.text = @"You have not scheduled any prayers for recital yet.";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:[UIFont labelFontSize]];
        [messageLabel sizeToFit];
        
        self.tableView.backgroundView = messageLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.eventIdentifiers count];
}

- (MGSwipeTableCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * reuseIdentifier = @"Cell";
    MGSwipeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if ( cell == nil )
        cell = [[MGSwipeTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    NSDictionary *lineItem = [self.schedules objectAtIndex:[indexPath row]];
    EKEvent *event = [lineItem objectForKey:@"event"];
    Chapter *chapter = [lineItem objectForKey:@"chapter"];
    
    FAKIcon *icon = [FAKIonIcons iosStopwatchOutlineIconWithSize:32];
    [icon setAttributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    [cell.imageView setImage:[icon imageWithSize:CGSizeMake(34, 34)]];
    
    NSAttributedString* attributedText = [[NSAttributedString alloc] initWithString:[chapter title] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Palatino-Italic" size:[UIFont labelFontSize]]}];
    [cell.textLabel setAttributedText:attributedText];

    
    if ( event.recurrenceRules == nil || [event.recurrenceRules count] == 0 ) {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@", [Utilities prettyDateAndTime:event.startDate], [Utilities prettyTime:event.endDate]];
    }
    else {
        
        EKRecurrenceRule *recurrenceRule = (EKRecurrenceRule *)[event.recurrenceRules firstObject];
        EKRecurrenceEnd *recurrenceEnd = [recurrenceRule recurrenceEnd];
        
        NSMutableArray *days = [NSMutableArray new];
        
        [[recurrenceRule daysOfTheWeek] each:^(EKRecurrenceDayOfWeek *object) {
            [days addObject:[kDaysOfWeek objectAtIndex:([object dayOfTheWeek] - 1)]];
        }];
        
        NSAttributedString* attributedDetailText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"Repeats weekly on %@ from %@ to %@",[days componentsJoinedByString:@", "], [Utilities prettyDateWithoutDay:event.startDate], [Utilities prettyDateWithoutDay:[recurrenceEnd endDate]]] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Palatino-Italic" size:[UIFont systemFontSize]]}];
        [cell.detailTextLabel setAttributedText:attributedDetailText];
        [cell.detailTextLabel setNumberOfLines:0];
    }
    
    
    FAKIcon *unscheduleIcon = [FAKIonIcons iosCloseOutlineIconWithSize:42];
    [unscheduleIcon setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    MGSwipeButton *unschedule = [MGSwipeButton buttonWithTitle:@"" icon:[unscheduleIcon imageWithSize:CGSizeMake(48,48)] backgroundColor:[UIColor redColor]];

    //configure right buttons
    cell.delegate = self;
    cell.rightButtons = @[unschedule];
    cell.rightSwipeSettings.transition = MGSwipeTransitionDrag;

    return cell;
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion {
    switch( direction ) {
        case MGSwipeDirectionRightToLeft:
            switch( index ) {
                case 0:
                {
                    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
                    NSDictionary *lineItem = [self.schedules objectAtIndex:[indexPath row]];
                    self.eventToDelete = [lineItem objectForKey:@"event"];
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirmation" message:@"Are you sure you want to remove this prayer from your schedule?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
                    [alert setTag:0];
                    [alert show];
                }
                    break;
                default:
                    break;
            }
            break;
        case MGSwipeDirectionLeftToRight:
            break;
        default:
            break;
    }
    
    return YES;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (alertView.tag) {
        case 0:
        {
            switch (buttonIndex) {
                case 0:
                {
                    PietaTabs *tabs = (PietaTabs *)self.tabBarController;
                    BOOL result = [tabs removeEvent:self.eventToDelete];
                    if ( !result ) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Could not remove prayer from your schedule" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles: nil];
                        [alertView setTag:1];
                        [alert show];
                    }
                    else {
                        
                        NSDictionary *schedules = [tabs getScheduledEvents];
                        self.schedules = [schedules allValues];
                        self.eventIdentifiers = [schedules allKeys];
                    }
                    
                    [self.tableView reloadData];
                }
                    break;
                default:
                    // No, remove the event selected for deletion
                    self.eventToDelete = nil;
                    break;
            }
        }
            break;
        default:
            break;
    }
    
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Set prompt to nil in case its still being displayed at time of view change
    [self.navigationItem setPrompt:nil];
    
    NSIndexPath *index = [self.tableView indexPathForSelectedRow];
    NSDictionary *lineItem = [self.schedules objectAtIndex:[index row]];
    
    Chapter *chapter = [lineItem objectForKey:@"chapter"];
    EKEvent *event = [lineItem objectForKey:@"event"];
    
    if ( [[segue identifier] isEqualToString:@"schedulerSegue"] ) {
        [[segue destinationViewController] setSchedule:chapter event:event];
    }
}

@end
