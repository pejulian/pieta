//
//  Bookmarks.m
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/9/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "Bookmarks.h"

@interface Bookmarks ()

@end

@implementation Bookmarks


- (void) awakeFromNib {
    
    CGFloat iconSize = [Utilities tabBarIconSize];
    FAKIcon *iconSelected = [FAKIonIcons iosStarIconWithSize:iconSize];
    FAKIcon *icon = [FAKIonIcons iosStarOutlineIconWithSize:iconSize];
    
    [iconSelected setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [icon setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    CGSize iconDim = [Utilities tabBarIconDimension];
    self.tabBarItem.image = [[icon imageWithSize:iconDim] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.tabBarItem.selectedImage = [[iconSelected imageWithSize:iconDim] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    self.tabBarItem.title = nil;
}

- (void) dealloc {
    
    self.book = nil;
    self.chapters = nil;
    self.bookmarks = nil;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 0.5);
    dispatch_after(delay, dispatch_get_main_queue(), ^(void) {
        [self.navigationItem setPrompt:@"Prayers that have been bookmarked are listed here.\nSwipe left on a prayer to remove it from your bookmarks"];
    });
    
    delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 3.0);
    dispatch_after(delay, dispatch_get_main_queue(), ^(void) {
        [self.navigationItem setPrompt:nil];
    });
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width - 150, self.navigationController.navigationBar.frame.size.height)];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [titleLabel setNumberOfLines:0];
    [titleLabel setTextColor:[UIColor whiteColor]];
    
    NSDictionary *titleAttr = @{
        NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:[UIFont labelFontSize]],
    };
    NSAttributedString* attributedTitle = [[NSAttributedString alloc] initWithString:@"Bookmarks" attributes:titleAttr];
    [titleLabel setAttributedText:attributedTitle];
    self.navigationItem.titleView = titleLabel;
    
    dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 0.5);
    dispatch_after(delay, dispatch_get_main_queue(), ^(void) {
        [self.navigationItem setPrompt:@"Swipe left to remove a prayer from your bookmarks"];
    });
    
    delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 3.0);
    dispatch_after(delay, dispatch_get_main_queue(), ^(void) {
        [self.navigationItem setPrompt:nil];
    });
    
    self.book = [Utilities book];
    self.chapters = [self.book chapters];
    self.bookmarks = [Utilities loadBookmarks];
    
    [self.tableView reloadData];
}


- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(preferredContentSizeChanged:) name:UIContentSizeCategoryDidChangeNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)preferredContentSizeChanged:(NSNotification *)notification {
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static UILabel* label;
    
    if (!label) {
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, FLT_MAX, FLT_MAX)];
        label.text = @"test";
    }
    
    [label setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleBody]];
    [label setNumberOfLines:0];
    [label sizeToFit];
    
    return label.frame.size.height * 3.7;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if ( [self.bookmarks count] > 0 ) {
        
        self.tableView.backgroundView = nil;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;

        return 1;
    }
    else {
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        messageLabel.text = @"You do not have any bookmarks yet.";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:[UIFont labelFontSize]];
        [messageLabel sizeToFit];
        
        self.tableView.backgroundView = messageLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.bookmarks count];
}

- (MGSwipeTableCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * reuseIdentifier = @"Cell";
    MGSwipeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if ( cell == nil )
        cell = [[MGSwipeTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    FAKIcon *icon = [FAKIonIcons iosStarOutlineIconWithSize:32];
    [icon setAttributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    [cell.imageView setImage:[icon imageWithSize:CGSizeMake(34, 34)]];
    
    NSInteger id = [(NSNumber *)[self.bookmarks objectAtIndex:[indexPath row]] integerValue];
    
    
    Chapter *chapter = (Chapter *) [self.chapters find:^BOOL(Chapter *chapter) {
        if ( [chapter id] == id ) {
            return chapter;
        }
        return nil;
    }];
    
    NSDictionary *titleAttr = @{NSFontAttributeName:[UIFont fontWithName:@"Palatino-Italic" size:[UIFont labelFontSize]]};
    NSAttributedString* attributedTitle = [[NSAttributedString alloc] initWithString:[chapter title] attributes:titleAttr];
    [cell.textLabel setAttributedText:attributedTitle];
    
    //configure right buttons
    FAKIcon *removeIcon = [FAKIonIcons iosMinusOutlineIconWithSize:42];
    [removeIcon setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    MGSwipeButton *remove = [MGSwipeButton buttonWithTitle:@"" icon:[removeIcon imageWithSize:CGSizeMake(48,48)] backgroundColor:[UIColor redColor]];
    
    cell.delegate = self;
    cell.rightButtons = @[remove];
    cell.rightSwipeSettings.transition = MGSwipeTransitionDrag;
    
    return cell;
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion {

    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSInteger id = [(NSNumber *)[self.bookmarks objectAtIndex:[indexPath row]] integerValue];
    Chapter *chapter = (Chapter *) [self.chapters find:^BOOL(Chapter *chapter) {
        if ( [chapter id] == id ) {
            return chapter;
        }
        return nil;
    }];
    
    switch( direction ) {
        case MGSwipeDirectionRightToLeft:
            switch( index ) {
                case 0:
                    if ( [Utilities removeChapterFromBookmarks:[chapter id]] ) {
                        dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 1.0);
                        dispatch_after(delay, dispatch_get_main_queue(), ^(void) {
                            [self.navigationItem setPrompt:@"Prayer removed from bookmarks"];
                        });
                        delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 3.0);
                        dispatch_after(delay, dispatch_get_main_queue(), ^(void) {
                            [self.navigationItem setPrompt:nil];
                        });
                    }
                    self.bookmarks = [Utilities loadBookmarks];
                    [self.tableView reloadData];
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    
    return YES;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    // Set prompt to nil in case its still being displayed at time of view change
    [self.navigationItem setPrompt:nil];
    
    NSIndexPath *index = [self.tableView indexPathForSelectedRow];
    NSInteger id = [(NSNumber *)[self.bookmarks objectAtIndex:[index row]] integerValue];
    Chapter *chapter = (Chapter *) [self.chapters find:^BOOL(Chapter *chapter) {
        if ( [chapter id] == id )
            return chapter;
        return nil;
    }];
    
    if ( [[segue identifier] isEqualToString:@"prayerDetailSegue"] ) {
        [[segue destinationViewController] setChapter:chapter];
    }
    else if ( [[segue identifier] isEqualToString:@"sharingPlatformsSegue"] ) {
        
        MZFormSheetController *formSheet = ((MZFormSheetSegue *)segue).formSheetController;
        [[segue destinationViewController] setChapter:chapter];
        formSheet.willPresentCompletionHandler = ^(UIViewController *vc) {
            vc.view.autoresizingMask = vc.view.autoresizingMask | UIViewAutoresizingFlexibleWidth;
        };
    }
    else {}
    
    
}
@end
