//
//  main.m
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/3/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
