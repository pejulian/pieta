//
//  PrayerOptions.m
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/28/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "PrayerOptions.h"

@interface PrayerOptions ()

@end

@implementation PrayerOptions

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.cellHeight = 80;
    
    self.tableView.backgroundColor = [[Utilities colors] objectForKey:@"tab_bar_bg"];

    CGSize iconDim = [Utilities navBarIconDimension];
    CGFloat iconSize = [Utilities navBarIconSize];
    
    FAKIcon *scheduleIcon = [FAKIonIcons iosStopwatchOutlineIconWithSize:iconSize];
    [scheduleIcon setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];

    FAKIcon *bookmarkIcon = [FAKIonIcons iosStarOutlineIconWithSize:iconSize];
    [bookmarkIcon setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    FAKIcon *emailIcon = [FAKIonIcons iosUploadOutlineIconWithSize:iconSize];
    [emailIcon setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];

    FAKIcon *searchIcon = [FAKIonIcons iosSearchIconWithSize:iconSize];
    [searchIcon setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    self.items  = [@{
        @0:@{
            @"icon": [scheduleIcon imageWithSize:iconDim],
            @"title": @"Schedule",
            @"subtitle": @"Add this chapter for recital into your prayer schedule"
        },
        @1:@{
            @"icon": [bookmarkIcon imageWithSize:iconDim],
            @"title": @"Bookmark",
            @"subtitle": @"Add this chapter to your bookmarks"
        },
        @2:@{
            @"icon": [emailIcon imageWithSize:iconDim],
            @"title": @"Email",
            @"subtitle": @"Email a PDF copy of this chapter"
        },
        @3:@{
            @"icon": [searchIcon imageWithSize:iconDim],
            @"title": @"Find word/phrase",
            @"subtitle": @"Search for a word or sentence in this chapter"
        }
    } mutableCopy];
}


- (void) setChapter:(Chapter *)chapter {
    
    if ( _chapter != chapter )
        _chapter = chapter;
    
    // Modifiy add bookmark
    if ( [Utilities isChapterInBookmarks:chapter.id] ) {
        
        CGFloat iconSize = [Utilities navBarIconSize];
        CGSize iconDim = [Utilities navBarIconDimension];

        FAKIcon *unbookmarkIcon = [FAKIonIcons iosStarIconWithSize:iconSize];
        [unbookmarkIcon setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        
        NSDictionary *item = @{
            @"icon": [unbookmarkIcon imageWithSize:iconDim],
            @"title": @"Remove bookmark",
            @"subtitle": @"Remove this chapter from your bookmarks"
        };
        
        [self.items removeObjectForKey:@1];
        
        [self.items setObject:item forKey:@1];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.allKeys.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * reuseIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if ( cell == nil )
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    NSDictionary *item = [self.items objectForKey:@([indexPath row])];
    
    cell.imageView.image = [item objectForKey:@"icon"];
    cell.textLabel.text = [item objectForKey:@"title"];
    cell.textLabel.font = [UIFont fontWithName:@"Optima-ExtraBlack" size:[UIFont labelFontSize]];
    cell.textLabel.textColor = [UIColor whiteColor];
    
    cell.detailTextLabel.text = [item objectForKey:@"subtitle"];
    cell.detailTextLabel.numberOfLines = 0;
    cell.detailTextLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:[UIFont labelFontSize]];
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    
    cell.backgroundColor = [[Utilities colors] objectForKey:@"tab_bar_bg"];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    switch ( indexPath.row ) {
        case 0:
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"scheduleChapter" object:nil];
        }
            break;
        case 1:
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"bookmarkChapter" object:nil];

        }
            break;
        case 2:
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"emailChapter" object:nil];
        }
            break;
        case 3:
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"findString" object:nil];
        }
            break;
        default:
            break;
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
