//
//  Bookmarks.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/9/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <FontAwesomeKit/FAKFontAwesome.h>
#import "FAKIonIcons.h"

#import <MGSwipeTableCell/MGSwipeTableCell.h>
#import <MGSwipeTableCell/MGSwipeButton.h>

#import <MZFormSheetController/MZFormSheetSegue.h>
#import <MZAppearance/MZAppearance.h>
#import <MZFormSheetController/MZFormSheetController.h>

#import "Utilities.h"
#import "Book.h"
#import "Chapter.h"

#import "PrayerDetail.h"

@interface Bookmarks : UITableViewController <MGSwipeTableCellDelegate>

@property (strong, nonatomic) Book *book;
@property (strong, nonatomic) NSArray<Chapter> *chapters;
@property (strong, nonatomic) NSArray *bookmarks;


@end
