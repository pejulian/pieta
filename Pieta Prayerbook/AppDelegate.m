//
//  AppDelegate.m
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/3/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    

    // Modal popups MZFormSheetController
    [[MZFormSheetController appearance] setCornerRadius:0];
    [[MZFormSheetController appearance] setShadowRadius:0];
    [[MZFormSheetController appearance] setShouldDismissOnBackgroundViewTap:YES];
    [[MZFormSheetController appearance] setTransitionStyle:MZFormSheetTransitionStyleSlideFromBottom];
    [[MZFormSheetBackgroundWindow appearance] setBackgroundBlurEffect:YES];
    [[MZFormSheetBackgroundWindow appearance] setBlurRadius:5.0];
    [[MZFormSheetBackgroundWindow appearance] setBackgroundColor:[UIColor clearColor]];
    
    [Utilities book];
    
    // Allocate the in-app email composer
    [APP cycleMailComposer];
    
    return YES;
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return NO;
}

- (void) cycleMailComposer {
    
    self.mailer = nil;
    self.mailer = [[MFMailComposeViewController alloc] init];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {


}



- (void)applicationDidBecomeActive:(UIApplication *)application {

}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
