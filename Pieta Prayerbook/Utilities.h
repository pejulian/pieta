//
//  Utilities.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/3/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ObjectiveSugar/ObjectiveSugar.h>

#import <Social/Social.h>

#import "Book.h"
#import "Chapter.h"

#import <YLMoment/YLMoment.h>

#import <CoreText/CoreText.h>

#import "SDiPhoneVersion.h"

#define kDaysOfWeek @[@"Sunday", @"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday"]
#define kElementTypes @[@"paragraph",@"points",@"centered",@"list",@"prayer",@"collection",@"footnote",@"image",@"numbered_list",@"response"]
#define kReminders @{@0: @"None", @5: @"5 mins", @15: @"15 mins", @30: @"30 mins", @60: @"1 hour"}

#define APP ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@interface Utilities : NSObject

+ (Book *) book;
+ (Chapter *) getChapterById:(NSInteger)id;

// Application color themes
+ (NSDictionary *) colors;

// iOS Version
+ (BOOL) isIOS8AndAbove;

// Icon sizes
+ (CGSize) tabBarIconDimension;
+ (CGFloat) tabBarIconSize;
+ (CGSize) navBarIconDimension;
+ (CGFloat) navBarIconSize;

// User preference management
+ (NSMutableDictionary *) loadUserDefaults;
+ (BOOL) saveUserDefaults:(NSMutableDictionary *) data;
+ (NSMutableArray *) loadBookmarks;
+ (NSMutableDictionary *) loadUserSchedule;
+ (BOOL) addChapterToBookmarks:(NSInteger) id;
+ (BOOL) addChapterToUserSchedule:(NSString *) eventIdentifier chapter:(NSInteger)chapterId;
+ (NSMutableArray *) getUserSchedule;
+ (BOOL) removeChapterFromBookmarks:(NSInteger) id;
+ (BOOL) removeChapterFromUserSchedule:(NSString *)id;
+ (BOOL) isChapterInBookmarks:(NSInteger) id;

// Date and time management
+ (YLMoment *) toMoment:(NSDate *)date;
+ (NSString *) prettyTime:(NSDate *) date;
+ (NSString *) prettyDate:(NSDate *) date;
+ (NSString *) prettyDateWithoutDay:(NSDate *) date;
+ (NSString *) prettyDateAndTime:(NSDate *) date;
+ (NSString *) prettyDateAndTimeWithoutDay:(NSDate *) date;
+ (NSDate *) addAmountOfTime:(NSDate *)date amount:(NSInteger)amount forCalendarUnit:(NSCalendarUnit)unit;
+ (NSDate *) subtractAmountOfTime:(NSDate *)date amount:(NSInteger)amount forCalendarUnit:(NSCalendarUnit)unit;
+ (NSDate *) setTimeToDate:(NSDate *)d time:(NSDate *)t;
+ (NSInteger) differenceBetweenTimes:(NSDate *)s end:(NSDate *)e forCalendarUnit:(NSCalendarUnit)unit;
+ (NSInteger) secondsToMinutes:(float)seconds;

// Sharing
+ (NSMutableString *) shareDescription;
+ (NSMutableString *) shareDescription: (NSString *) name;

// Rich text rendering procedures
+ (NSTextStorage *) buildTextForChapter:(Chapter *) chapter;
+ (NSAttributedString *)createAttributedString:(NSString*)style withTrait:(uint32_t)trait withText:(NSString *)text;
+ (NSDictionary *)buildAttributes:(NSString*)style withTrait:(uint32_t)trait;
+ (UIImage *) imageWithImage: (UIImage *) sourceImage scaledToWidth:(float) i_width;

@end
