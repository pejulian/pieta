//
//  PrayerOptions.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/28/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <FontAwesomeKit/FAKFontAwesome.h>
#import "FAKIonIcons.h"

#import "Utilities.h"

@interface PrayerOptions : UITableViewController

@property (strong, nonatomic) Chapter *chapter;
@property (strong, nonatomic) NSMutableDictionary *items;
@property (assign, nonatomic) float cellHeight;

@end
