//
//  PietaNav.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/27/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Utilities.h"

@interface PietaNav : UINavigationController

@end
