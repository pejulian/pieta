//
//  Content.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/3/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

#import "Element.h"

@protocol Content @end

@interface Content : JSONModel

@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSArray<Element>* body;

@end
