//
//  Utilities.m
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/3/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "Utilities.h"

@implementation Utilities

static Book *book = nil;

+ (Book *) book {
    
    if ( book == nil ) {
        
        NSError *error = nil;
        NSString *path = [[NSBundle mainBundle] pathForResource:@"pieta" ofType:@"json"];
        NSString *json = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
        
        NSLog(@"JSON path: %@", path);
        
        if ( error == nil )
            book = [[Book alloc] initWithString:json error:&error];
    }
    return book;
}

+ (Chapter *) getChapterById:(NSInteger)id {
    
    Book *book = [Utilities book];
    NSArray<Chapter> *chapters = [book chapters];
    
    return [chapters find:^BOOL(Chapter *c) {
        return (c.id == id );
    }];
}

+ (NSDictionary *) colors {
    return @{
        @"background_gray": [UIColor colorWithRed:86/255.0f green:76/255.0f blue:98/255.0f alpha:1],
        @"tab_bar_bg": [UIColor colorWithRed:0/255.0f green:156/255.0f blue:222/255.0f alpha:1]
    };
}

+ (BOOL) isIOS8AndAbove {
    return iOSVersionGreaterThanOrEqualTo(@"8");
}

+ (CGFloat) tabBarIconSize {
    
    CGFloat iconSize;
    
    // iPhone 6 Plus
    if ( [SDiPhoneVersion deviceVersion] == iPhone6Plus  ) {
        iconSize = 42;
    }
    // iPhone 6
    else if ( [SDiPhoneVersion deviceVersion] == iPhone6 ) {
        iconSize = 36;
    }
    // iPhone 5s, 5c 5, 4s or 4 / iPad Mini Retina, Ipad Air, iPad 4
    else if ( [SDiPhoneVersion deviceVersion] == iPhone5S || [SDiPhoneVersion deviceVersion] == iPhone5C || [SDiPhoneVersion deviceVersion] == iPhone5 || [SDiPhoneVersion deviceVersion] == iPhone4 || [SDiPhoneVersion deviceVersion] == iPhone4S || [SDiPhoneVersion deviceVersion] == iPadMiniRetina || [SDiPhoneVersion deviceVersion] == iPadAir || [SDiPhoneVersion deviceVersion] == iPad4 ) {
        iconSize = 32;
    }
    // iPad Mini and iPad 2
    else if ( [SDiPhoneVersion deviceVersion] == iPadMini || [SDiPhoneVersion deviceVersion] == iPad2 || [SDiPhoneVersion deviceVersion] == iPad1 ) {
        iconSize = 24;
    }
    // Unhandled iPad 3, iPhone 3 and below (or simulator)
    else {
        iconSize = 36;
    }
    
    return iconSize;
}

+ (CGSize) tabBarIconDimension {
    
    CGSize iconDim;
    
    // iPhone 6 Plus
    if ( [SDiPhoneVersion deviceVersion] == iPhone6Plus  ) {
        NSLog(@"tab bar icon size: 144 x 96");
        iconDim = CGSizeMake(144, 96);
    }
    // iPhone 6
    else if ( [SDiPhoneVersion deviceVersion] == iPhone6 ) {
        NSLog(@"tab bar icon size: 96 x 64");
        iconDim = CGSizeMake(96, 64);
    }
    // iPhone 5s, 5c 5, 4s or 4 / iPad Mini Retina, Ipad Air, iPad 4
    else if ( [SDiPhoneVersion deviceVersion] == iPhone5S || [SDiPhoneVersion deviceVersion] == iPhone5C || [SDiPhoneVersion deviceVersion] == iPhone5 || [SDiPhoneVersion deviceVersion] == iPhone4 || [SDiPhoneVersion deviceVersion] == iPhone4S || [SDiPhoneVersion deviceVersion] == iPadMiniRetina || [SDiPhoneVersion deviceVersion] == iPadAir || [SDiPhoneVersion deviceVersion] == iPad4 ) {
        NSLog(@"tab bar icon size: 96 x 64");
        iconDim = CGSizeMake(96, 64);
    }
    // iPad Mini and iPad 2, iPad 1
    else if ( [SDiPhoneVersion deviceVersion] == iPadMini || [SDiPhoneVersion deviceVersion] == iPad2 || [SDiPhoneVersion deviceVersion] == iPad1 ) {
        NSLog(@"tab bar icon size: 48 x 32");
        iconDim = CGSizeMake(48, 32);
    }
    // Unhandled iPad 3, iPhone 3 and below (or simulator)
    else {
        NSLog(@"tab bar icon size: 96 x 64");
        iconDim = CGSizeMake(96, 64);
    }
    return iconDim;
}

+ (CGSize) navBarIconDimension {
    
    CGSize iconDim;
    
    // iPhone 6 Plus
    if ( [SDiPhoneVersion deviceVersion] == iPhone6Plus  ) {
        NSLog(@"nav bar icon size: 66 x 66");
        iconDim = CGSizeMake(66, 66);
    }
    // iPhone 6
    else if ( [SDiPhoneVersion deviceVersion] == iPhone6 ) {
        NSLog(@"nav bar icon size: 44 x 44");
        iconDim = CGSizeMake(44, 44);
    }
    // iPhone 5s, 5c 5, 4s or 4 / iPad Mini Retina, Ipad Air, iPad 4
    else if ( [SDiPhoneVersion deviceVersion] == iPhone5S || [SDiPhoneVersion deviceVersion] == iPhone5C || [SDiPhoneVersion deviceVersion] == iPhone5 || [SDiPhoneVersion deviceVersion] == iPhone4 || [SDiPhoneVersion deviceVersion] == iPhone4S || [SDiPhoneVersion deviceVersion] == iPadMiniRetina || [SDiPhoneVersion deviceVersion] == iPadAir || [SDiPhoneVersion deviceVersion] == iPad4 ) {
        NSLog(@"nav bar icon size: 44 x 44");
        iconDim = CGSizeMake(44, 44);
    }
    // iPad Mini and iPad 2, iPad 1
    else if ( [SDiPhoneVersion deviceVersion] == iPadMini || [SDiPhoneVersion deviceVersion] == iPad2 || [SDiPhoneVersion deviceVersion] == iPad1 ) {
        NSLog(@"nav bar icon size: 22 x 22");
        iconDim = CGSizeMake(22, 22);
    }
    // Unhandled iPad 3, iPhone 3 and below (or simulator)
    else {
        NSLog(@"nav bar icon size: 44 x 44");
        iconDim = CGSizeMake(44, 44);
    }
    
    return iconDim;
}

+ (CGFloat) navBarIconSize {
    
    CGFloat iconSize;
    
    // iPhone 6 Plus
    if ( [SDiPhoneVersion deviceVersion] == iPhone6Plus  ) {
        iconSize = 42;
    }
    // iPhone 6
    else if ( [SDiPhoneVersion deviceVersion] == iPhone6 ) {
        iconSize = 32;
    }
    // iPhone 5s, 5c 5, 4s or 4 / iPad Mini Retina, Ipad Air, iPad 4
    else if ( [SDiPhoneVersion deviceVersion] == iPhone5S || [SDiPhoneVersion deviceVersion] == iPhone5C || [SDiPhoneVersion deviceVersion] == iPhone5 || [SDiPhoneVersion deviceVersion] == iPhone4 || [SDiPhoneVersion deviceVersion] == iPhone4S || [SDiPhoneVersion deviceVersion] == iPadMiniRetina || [SDiPhoneVersion deviceVersion] == iPadAir || [SDiPhoneVersion deviceVersion] == iPad4 ) {
        iconSize = 32;
    }
    // iPad Mini and iPad 2, iPad 1
    else if ( [SDiPhoneVersion deviceVersion] == iPadMini || [SDiPhoneVersion deviceVersion] == iPad2 || [SDiPhoneVersion deviceVersion] == iPad1 ) {
        iconSize = 24;
    }
    // Unhandled iPad 3, iPhone 3 and below (or simulator)
    else {
        iconSize = 32;
    }
    
    return iconSize;
}

+ (YLMoment *) toMoment:(NSDate *)date {
    return [YLMoment momentWithDate:date];
}

+ (NSString *) prettyTime:(NSDate *) date {
    YLMoment *moment = [Utilities toMoment:date];
    return [moment format:@"h:mm a"];
}

+ (NSString *) prettyDate:(NSDate *) date {
    YLMoment *moment = [Utilities toMoment:date];
    return [moment format:@"EEE, MMM dd yyyy"];
}

+ (NSString *) prettyDateWithoutDay:(NSDate *) date {
    YLMoment *moment = [Utilities toMoment:date];
    return [moment format:@"MMM dd yyyy"];
}

+ (NSString *) prettyDateAndTime:(NSDate *) date {
    YLMoment *moment = [Utilities toMoment:date];
    return [moment format:@"EEE, MMMM dd yyyy, h:mm a"];
}

+ (NSString *) prettyDateAndTimeWithoutDay:(NSDate *) date {
    YLMoment *moment = [Utilities toMoment:date];
    return [moment format:@"MMMM dd yyyy, h:mm a"];
}

+ (NSDate *) addAmountOfTime:(NSDate *)date amount:(NSInteger)amount forCalendarUnit:(NSCalendarUnit)unit {
    YLMoment *moment = [Utilities toMoment:date];
    YLMoment *nextMoment = [moment addAmountOfTime:amount forCalendarUnit:unit];
    return [nextMoment date];
}

+ (NSDate *) subtractAmountOfTime:(NSDate *)date amount:(NSInteger)amount forCalendarUnit:(NSCalendarUnit)unit {
    YLMoment *moment = [Utilities toMoment:date];
    YLMoment *previousMoment = [moment subtractAmountOfTime:amount forCalendarUnit:unit];
    return [previousMoment date];
}

+ (NSInteger) differenceBetweenTimes:(NSDate *)s end:(NSDate *)e forCalendarUnit:(NSCalendarUnit)unit {
    NSTimeInterval secondsBetween = [e timeIntervalSinceDate:s];
    switch ( unit ) {
        case NSCalendarUnitMinute:
            return secondsBetween /60;
            break;
        case NSCalendarUnitSecond:
            return secondsBetween;
            break;
        case NSCalendarUnitHour:
            return secondsBetween / 3600;
            break;
        default:
            return secondsBetween;
            break;
    }
}

+ (NSDate *) setTimeToDate:(NSDate *)d time:(NSDate *)t {
    
    YLMoment *date = [Utilities toMoment:d];
    YLMoment *time = [Utilities toMoment:t];
    
    [date setHour:[time hour]];
    [date setMinute:[time minute]];
    
    return [date date];
}

+ (NSInteger) secondsToMinutes:(float)seconds {
    NSInteger minutes = seconds / 60;
    NSNumber *num = [NSNumber numberWithInteger:minutes];
    return abs([num intValue]);
}


+ (NSMutableDictionary *) defineNewUserPreferences {
    NSMutableDictionary *data = [NSMutableDictionary new];
    [data setValue:[NSMutableArray new] forKey:@"bookmarks"];
    [data setValue:[NSMutableArray new] forKey:@"schedule"];
    return data;
}

+ (NSMutableDictionary *) loadUserDefaults {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *data = [[defaults objectForKey:@"pieta_userprefs"] mutableCopy];
    if ( data == nil ) {
        data = [Utilities defineNewUserPreferences];
        [Utilities saveUserDefaults:data];
    }
    return data;
}

+ (BOOL) saveUserDefaults:(NSMutableDictionary *) data {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:data forKey:@"pieta_userprefs"];
    return YES;
}

+ (NSMutableArray *) loadBookmarks {
    NSMutableDictionary *data = [Utilities loadUserDefaults];
    NSMutableArray *favourites = [[data objectForKey:@"bookmarks"] mutableCopy];
    return favourites;
}

+ (NSDictionary *) loadUserSchedule {
    NSMutableDictionary *data = [Utilities loadUserDefaults];
    NSMutableDictionary *schedule = [[data objectForKey:@"schedule"] mutableCopy];
    return schedule;
}

+ (BOOL) isChapterInBookmarks:(NSInteger) id {
    NSMutableArray *favourites = [Utilities loadBookmarks];
    return [favourites containsObject:[NSNumber numberWithInteger:id]];
}

+ (BOOL) addChapterToBookmarks:(NSInteger) id {
    NSMutableDictionary *data = [Utilities loadUserDefaults];
    NSMutableArray *bookmarks = [[data objectForKey:@"bookmarks"] mutableCopy];
    [bookmarks addObject:[NSNumber numberWithInteger:id]];
    [data setValue:bookmarks forKey:@"bookmarks"];
    return [Utilities saveUserDefaults:data];
}

+ (BOOL) addChapterToUserSchedule:(NSString *) eventIdentifier chapter:(NSInteger)chapterId {
    NSMutableDictionary *data = [Utilities loadUserDefaults];
    NSMutableArray *schedule = [[data objectForKey:@"schedule"] mutableCopy];
    [schedule addObject:@{@"eventIdentifier": eventIdentifier, @"chapterId": [NSNumber numberWithInteger:chapterId] }];
    [data setValue:schedule forKey:@"schedule"];
    return [Utilities saveUserDefaults:data];
}

+ (BOOL) removeChapterFromBookmarks:(NSInteger) id {
    NSMutableDictionary *data = [Utilities loadUserDefaults];
    NSMutableArray *bookmarks = [[data objectForKey:@"bookmarks"] mutableCopy];
    [bookmarks removeObject:[NSNumber numberWithInteger:id]];
    [data setValue:bookmarks forKey:@"bookmarks"];
    return [Utilities saveUserDefaults:data];
}

+ (NSMutableArray *) getUserSchedule {
    NSMutableDictionary *data = [Utilities loadUserDefaults];
    NSMutableArray *schedule = [[data objectForKey:@"schedule"] mutableCopy];
    return schedule;
}

+ (BOOL) removeChapterFromUserSchedule:(NSString *) eventIdentifier {
    NSMutableDictionary *data = [Utilities loadUserDefaults];
    NSMutableArray *schedule = [[data objectForKey:@"schedule"] mutableCopy];
    schedule = [[schedule reject:^BOOL(NSDictionary *lineItem) {
        return [eventIdentifier isEqualToString:[lineItem valueForKey:@"eventIdentifier"]];
    }] mutableCopy];
    [data setValue:schedule forKey:@"schedule"];
    return [Utilities saveUserDefaults:data];
}

+ (NSMutableString *) shareDescription {
    
    NSMutableString *body = [NSMutableString new];

    [body appendString:@"Shalom!\n\n"];
    [body appendString:@"I would like to share this prayer from the Pieta Prayerbook with you!"];

    return body;
}

+ (NSMutableString *) shareDescription: (NSString *) name {
    
    NSMutableString *body = [NSMutableString new];
    
    [body appendString:@"Shalom!\n\n"];
    [body appendString:@"I would like to share this prayer from the Pieta Prayerbook with you!\n\n"];
    [body appendString:[NSString stringWithFormat:@"Simply open the file named \"%@\" attached here.\n\n", name]];
    [body appendString:[NSString stringWithFormat:@"Thanks & God bless,\n"]];
    [body appendString:[NSString stringWithFormat:@"The Pieta Prayerbook"]];
    
    return body;
}

+ (NSTextStorage *) buildTextForChapter:(Chapter *) chapter {
    
    NSArray<Content> *contents = [chapter contents];
    
    // =======================================
    // Text view with container setup
    // =======================================
    NSTextStorage *textStorage = [NSTextStorage new];
    
    // =======================================
    // Paragraph styles
    // =======================================
    
    NSMutableParagraphStyle *justifiedStyle = [NSMutableParagraphStyle new];
    justifiedStyle.alignment = NSTextAlignmentJustified;
    justifiedStyle.headIndent = 5.0;
    justifiedStyle.firstLineHeadIndent = 20.0;
    justifiedStyle.tailIndent = -5.0;
    
    NSMutableParagraphStyle *titleStyle = [NSMutableParagraphStyle new];
    titleStyle.alignment = NSTextAlignmentLeft;
    titleStyle.firstLineHeadIndent = 5.0;
    titleStyle.headIndent = 5.0;
    titleStyle.tailIndent = -5.0;
    
    NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
    [paragrahStyle setParagraphSpacing:4];
    [paragrahStyle setParagraphSpacingBefore:3];
    [paragrahStyle setFirstLineHeadIndent:0.0f];  // First line is the one with bullet point
    [paragrahStyle setHeadIndent:10.5f];    // Set the indent for given bullet character and size font
    
    NSMutableParagraphStyle *listStyle = [NSMutableParagraphStyle new];
    listStyle.paragraphSpacing = 5;
    listStyle.paragraphSpacingBefore = 4;
    listStyle.firstLineHeadIndent = 8.5f; // First line with number
    listStyle.headIndent = 20.0f; // indent for bullet character
    listStyle.alignment = NSTextAlignmentLeft;

    NSMutableParagraphStyle *centerStyle = [NSMutableParagraphStyle new];
    centerStyle.alignment = NSTextAlignmentCenter;
    
    NSMutableParagraphStyle *rightStyle = [NSMutableParagraphStyle new];
    rightStyle.tailIndent = -5.0;
    rightStyle.alignment = NSTextAlignmentRight;
    
    // =======================================
    // Subtitle
    // =======================================
    
    if ([chapter subtitle] != nil && [[chapter subtitle] length] > 0 ) {
        NSMutableDictionary *subtitleAttrs = [[NSMutableDictionary alloc] initWithDictionary:[self buildAttributes:UIFontTextStyleSubheadline withTrait:UIFontDescriptorTraitItalic]];
        [subtitleAttrs setValue:centerStyle forKey:NSParagraphStyleAttributeName];
        [textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", [chapter subtitle]] attributes:subtitleAttrs]];
    }
    
    // =======================================
    // Author
    // =======================================
    
    if ([chapter author] != nil && [[chapter author] length] > 0 ) {
        NSMutableDictionary *authorAttrs = [[NSMutableDictionary alloc] initWithDictionary:[self buildAttributes:UIFontTextStyleCaption1 withTrait:UIFontDescriptorTraitItalic]];
        [authorAttrs setValue:centerStyle forKey:NSParagraphStyleAttributeName];
        [textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", [chapter author]] attributes:authorAttrs]];
    }
    
    [contents eachWithIndex:^(Content *content, NSUInteger indx) {
        
        NSArray<Element>* body = [content body];
        
        if ( [content title] != nil && [[content title] length] > 0 ) {
            
            NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:@{NSParagraphStyleAttributeName : titleStyle, NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]}];
            [attributes setValue:@(NSUnderlineStyleSingle) forKey:NSUnderlineStyleAttributeName];
            [textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@\n", [content title]] attributes:attributes]];
        }
        
        [body each:^(Element *element) {
            
            if ( [element title] != nil ) {
                
                // paragraph
                if ( [[element type] isEqualToString:[kElementTypes objectAtIndex:0]] ) {
                    
                    if ( [element title] != nil ) {
                        
                        NSMutableString *text = [NSMutableString new];
                        [text appendString:@"\n"];
                        [text appendString:[element title]];
                        [text appendString:@"\n"];
                        
                        NSDictionary *attributes = @{NSParagraphStyleAttributeName : justifiedStyle, NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleBody]};
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                        [textStorage appendAttributedString:attributedString];
                    }
                }
                
                // points
                else if ( [[element type] isEqualToString:[kElementTypes objectAtIndex:1]] ) {
                    
                    NSString *title = [element title];
                    NSArray *items = [element items];
                    
                    if ( title != nil && [title length] > 0) {
                        
                        NSMutableString *text = [NSMutableString new];
                        [text appendString:@"\n"];
                        [text appendString:[element title]];
                        [text appendString:@"\n"];
                        
                        NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:[self buildAttributes:UIFontTextStyleBody withTrait:UIFontDescriptorTraitBold]];
                        [attributes setValue:titleStyle forKey:NSParagraphStyleAttributeName];
                        //[attributes setValue:@(NSUnderlineStyleSingle) forKey:NSUnderlineStyleAttributeName];
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                        [textStorage appendAttributedString:attributedString];
                    }
                    
                    [items eachWithIndex:^(NSString *item, NSUInteger index) {
                        
                        NSMutableString *text = [NSMutableString new];
                        
                        if ( index == 0 )
                            [text appendString:@"\n"];
                        
                        [text appendString:item];
                        [text appendString:@"\n"];
                        
                        if ( index == [items count]  )
                            [text appendString:@"\n"];
                        
                        NSDictionary *attributes = @{NSParagraphStyleAttributeName : titleStyle, NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleBody]};
                        
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                        [textStorage appendAttributedString:attributedString];
                    }];
                }
                
                // centered
                else if ( [[element type] isEqualToString:[kElementTypes objectAtIndex:2]] ) {
                    
                    NSArray *items = [element items];
                    [items eachWithIndex:^(NSString *item, NSUInteger index) {
                        
                        NSMutableString *text = [NSMutableString new];
                        if ( index == 0 ) {
                            
                            [text appendString:@"\n"];
                            [text appendString:item];
                            [text appendString:@"\n"];
                        }
                        else {
                            
                            [text appendString:item];
                            [text appendString:@"\n"];
                        }
                        
                        NSDictionary *attributes = @{NSParagraphStyleAttributeName : centerStyle, NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleBody]};
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                        [textStorage appendAttributedString:attributedString];
                    }];
                }
                
                // list
                else if ( [[element type] isEqualToString:[kElementTypes objectAtIndex:3]] ) {
                    
                    NSString *title = [element title];
                    NSArray *items = [element items];
                    
                    if ( title != nil ) {
                        
                        NSMutableString *text = [NSMutableString new];
                        [text appendString:@"\n"];
                        
                        if ( [title length] > 0 ) {
                            [text appendString:[element title]];
                            [text appendString:@"\n"];
                        }
                        
                        NSDictionary *attributes = [self buildAttributes:UIFontTextStyleBody withTrait:UIFontDescriptorTraitBold];
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                        [textStorage appendAttributedString:attributedString];
                    }
                    
                    [items eachWithIndex:^(NSString *item, NSUInteger index) {
                        
                        NSMutableString *text = [NSMutableString new];
                        [text appendFormat:@"\u2022 %@\n", item];
                        
                        NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:[self buildAttributes:UIFontTextStyleBody withTrait:UIFontDescriptorTraitLooseLeading]];
                        
                        [attributes setValue:listStyle forKey:NSParagraphStyleAttributeName];
                        
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                        [textStorage appendAttributedString:attributedString];
                    }];
                }
                
                // prayer
                else if ( [[element type] isEqualToString:[kElementTypes objectAtIndex:4]] ) {
                    
                    NSArray *items = [element items];
                    NSString *title = [element title];
                    
                    if ( title != nil ) {
                        
                        NSMutableString *text = [NSMutableString new];
                        
                        if ( [title length] > 0 ) {
                            [text appendString:[element title]];
                            [text appendString:@"\n"];
                        }
                        
                        NSDictionary *attributes = [self buildAttributes:UIFontTextStyleBody withTrait:UIFontDescriptorTraitBold];
                        
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                        [textStorage appendAttributedString:attributedString];
                    }
                    
                    [items eachWithIndex:^(NSString *item, NSUInteger index) {
                        
                        NSMutableString *text = [NSMutableString new];
                        
                        if ( index == 0 )
                            [text appendString:@"\n"];
                        
                        [text appendString:item];
                        [text appendString:@"\n\n"];
                        
                        if ( index == 0 ) {
                            
                            NSMutableDictionary *attributes = [[self buildAttributes:UIFontTextStyleBody withTrait:UIFontDescriptorTraitBold] mutableCopy];
                            [attributes setValue:justifiedStyle forKey:NSParagraphStyleAttributeName];
                            NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                            [textStorage appendAttributedString:attributedString];
                            
                            
                        }
                        else {
                            
                            
                            NSDictionary *attributes = @{NSParagraphStyleAttributeName : justifiedStyle, NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleBody]};
                            NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                            [textStorage appendAttributedString:attributedString];
                            
                            
                        }
                    }];
                }
                
                // collection
                else if ( [[element type] isEqualToString:[kElementTypes objectAtIndex:5]] ) {
                    
                    NSArray *items = [element items];
                    NSString *title = [element title];
                    
                    if ( title != nil ) {
                        
                        NSMutableString *text = [NSMutableString new];
                        [text appendString:@"\n"];
                        [text appendString:[element title]];
                        [text appendString:@"\n"];
                        
                        NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:[self buildAttributes:UIFontTextStyleBody withTrait:UIFontDescriptorTraitBold]];
                        [attributes setValue:centerStyle forKey:NSParagraphStyleAttributeName];
                        [attributes setValue:@(NSUnderlineStyleSingle) forKey:NSUnderlineStyleAttributeName];
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                        [textStorage appendAttributedString:attributedString];
                    }
                    
                    [items eachWithIndex:^(NSArray *item, NSUInteger index) {
                        
                        NSAttributedString *attrString = [self createAttributedString:UIFontTextStyleBody withTrait:UIFontDescriptorTraitItalic withText:@"\n"];
                        [textStorage appendAttributedString:attrString];
                        
                        [item eachWithIndex:^(NSString *point, NSUInteger idx) {
                            
                            NSMutableString *text = [NSMutableString new];
                            [text appendString:point];
                            [text appendString:@"\n"];
                            
                            NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:[self buildAttributes:UIFontTextStyleBody withTrait:UIFontDescriptorTraitItalic]];
                            [attributes setValue:centerStyle forKey:NSParagraphStyleAttributeName];
                            NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                            [textStorage appendAttributedString:attributedString];
                        }];
                    }];
                }
                
                // footnote
                else if ( [[element type] isEqualToString:[kElementTypes objectAtIndex:6]] ) {
                    
                    NSMutableString *text = [NSMutableString new];
                    [text appendString:@"\n"];
                    [text appendString:[element title]];
                    [text appendString:@"\n"];
                    
                    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:[self buildAttributes:UIFontTextStyleFootnote withTrait:UIFontDescriptorTraitBold]];
                    [attributes setValue:centerStyle forKey:NSParagraphStyleAttributeName];
                    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                    [textStorage appendAttributedString:attributedString];
                }
                
                // image
                else if ( [[element type] isEqualToString:[kElementTypes objectAtIndex:7]] ) {
                    
                    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:[self buildAttributes:UIFontTextStyleFootnote withTrait:UIFontDescriptorTraitItalic]];
                    [attributes setValue:centerStyle forKey:NSParagraphStyleAttributeName];
                    [textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n" attributes:attributes]];
                    
                    // Add inline image as attachment
                    CGRect screenBounds = [[UIScreen mainScreen] bounds];
                    float width = screenBounds.size.width * 0.70f;
                    UIImage *image = [UIImage imageNamed:[element title]];

                    // Scale down the image if it is larger than the current screen size
                    if ( image.size.width > screenBounds.size.width )
                        image = [self imageWithImage:[UIImage imageNamed:[element title]] scaledToWidth:width];
                
                    // Create a text attachment object and append the image object to it
                    NSTextAttachment *imageAttachment = [[NSTextAttachment alloc] initWithData:nil ofType:nil];
                    imageAttachment.image = image;

                    NSMutableAttributedString *attributedImageAttachment = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:imageAttachment]];
                    [attributedImageAttachment addAttribute:NSParagraphStyleAttributeName value:centerStyle range:NSMakeRange(0, [attributedImageAttachment length]) ];
                    [textStorage appendAttributedString:attributedImageAttachment];
                    
                    [textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n" attributes:attributes]];
                    
                    // Add the image caption
                    NSMutableString *text = [NSMutableString new];
                    NSArray *items = [element items];
                    [items eachWithIndex:^(NSString *item, NSUInteger index) {
                        
                        if ( index == 0 ) {
                            [text appendString:@"\n"];
                            [text appendString:item];
                        }
                        else
                            [text appendString:item];
                        
                        [text appendString:@"\n"];

                    }];
                                        
                    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                    [textStorage appendAttributedString:attributedString];
                }
                
                
                // numbered_list
                else if ( [[element type] isEqualToString:[kElementTypes objectAtIndex:8]] ) {

                    NSString *title = [element title];
                    NSArray *items = [element items];
                    
                    if ( title != nil ) {
                        
                        NSMutableString *text = [NSMutableString new];
                        [text appendString:@"\n"];
                        
                        if ( [title length] > 0 ) {
                            [text appendString:[element title]];
                            [text appendString:@"\n"];
                        }
                        
                        NSDictionary *attributes = [self buildAttributes:UIFontTextStyleBody withTrait:UIFontDescriptorTraitBold];
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                        [textStorage appendAttributedString:attributedString];
                    }
                    
                    [items eachWithIndex:^(NSString *item, NSUInteger index) {
                        
                        NSMutableString *text = [NSMutableString new];
                        [text appendFormat:@"%li %@\n", (index + 1), item];
                        
                        NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:[self buildAttributes:UIFontTextStyleBody withTrait:UIFontDescriptorTraitLooseLeading]];
                        
                        [attributes setValue:listStyle forKey:NSParagraphStyleAttributeName];
                        
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                        [textStorage appendAttributedString:attributedString];
                    }];
                }
                
                // response
                else if ( [[element type] isEqualToString:[kElementTypes objectAtIndex:9]] ) {
                    
                    NSString *title = [element title];
                    NSArray *items = [element items];
                    
                    if ( title != nil ) {
                        
                        NSMutableString *text = [NSMutableString new];
                        [text appendString:@"\n"];
                        
                        if ( [title length] > 0 ) {
                            [text appendString:[element title]];
                            [text appendString:@"\n"];
                        }
                        
                        NSDictionary *attributes = [self buildAttributes:UIFontTextStyleBody withTrait:UIFontDescriptorTraitBold];
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                        [textStorage appendAttributedString:attributedString];
                    }
                    
                    NSArray *prefixes = [NSArray arrayWithObjects:@"V.",@"R.", nil];
                    
                    [items eachWithIndex:^(NSString *item, NSUInteger index) {
                        
                        NSMutableString *text = [NSMutableString new];
                        [text appendFormat:@"%@ %@\n", [prefixes objectAtIndex:(index%2)], item];
                        
                        NSMutableDictionary *attributes = [[NSMutableDictionary alloc] initWithDictionary:[self buildAttributes:UIFontTextStyleBody withTrait:UIFontDescriptorTraitLooseLeading]];
                        
                        [attributes setValue:listStyle forKey:NSParagraphStyleAttributeName];
                        
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] attributes:attributes];
                        [textStorage appendAttributedString:attributedString];
                    }];
                }
                
                else {}
            }
        }];
    }];
    
    // ======================================================
    // Inline formatting of text (find patterns and replace)
    // ======================================================
    
    // create the attributes
    // NSDictionary* boldAttributes = [self buildAttributes:UIFontTextStyleBody withTrait:UIFontDescriptorTraitBold];
    NSDictionary *italicAttributes = [self buildAttributes:UIFontTextStyleBody withTrait:UIFontDescriptorTraitItalic];
    NSMutableDictionary *emailAttributes = [[self buildAttributes:UIFontTextStyleBody withTrait:UIFontDescriptorTraitItalic] mutableCopy];
    [emailAttributes setValue:@(NSUnderlineStyleSingle) forKey:NSUnderlineStyleAttributeName];

    NSDictionary *patterns = @{
        @"(\"[^\"]*\")": italicAttributes,                // Text within quotes
    };
    
    for ( NSString* key in patterns ) {
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:key options:0 error:nil];
        NSDictionary* attributes = patterns[key];
        
        [regex enumerateMatchesInString:[textStorage string] options:0 range:NSMakeRange(0, [[textStorage string] length]) usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop){
            
            // apply the style
            NSRange matchRange = [match rangeAtIndex:1];
            [textStorage addAttributes:attributes range:matchRange];
            
        }];
    }
    
    return textStorage;
}

+ (NSAttributedString *)createAttributedString:(NSString*)style withTrait:(uint32_t)trait withText:(NSString *)text {
    
    UIFontDescriptor *fontDescriptor = [UIFontDescriptor preferredFontDescriptorWithTextStyle:style];
    UIFontDescriptor *descriptorWithTrait = [fontDescriptor fontDescriptorWithSymbolicTraits:trait];
    UIFont* font = [UIFont fontWithDescriptor:descriptorWithTrait size: 0.0];
    NSDictionary *attributes = @{ NSFontAttributeName: font };
    NSAttributedString *string = [[NSAttributedString alloc] initWithString:text attributes:attributes];
    return string;
}

+ (NSDictionary *)buildAttributes:(NSString*)style withTrait:(uint32_t)trait {
    
    UIFontDescriptor *fontDescriptor = [UIFontDescriptor preferredFontDescriptorWithTextStyle:style];
    UIFontDescriptor *descriptorWithTrait = [fontDescriptor fontDescriptorWithSymbolicTraits:trait];
    UIFont* font = [UIFont fontWithDescriptor:descriptorWithTrait size: 0.0];
    return @{ NSFontAttributeName: font };
}

+ (UIImage *) imageWithImage: (UIImage *) sourceImage scaledToWidth:(float) i_width {
    
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(newWidth, newHeight), YES, 2.0);
        } else {
            UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
        }
    } else {
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    }
    
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
