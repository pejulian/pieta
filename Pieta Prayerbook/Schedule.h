//
//  Schedule.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/12/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import <EventKit/EventKit.h>

#import "Chapter.h"

@protocol Schedule @end

@interface Schedule : JSONModel

@property (assign, nonatomic) NSInteger id;
@property (assign, nonatomic) NSInteger chapterId;

@property (strong, nonatomic) NSArray *events;


@end
