//
//  MySchedule.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/8/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ObjectiveSugar/ObjectiveSugar.h>

#import <MGSwipeTableCell/MGSwipeTableCell.h>
#import <MGSwipeTableCell/MGSwipeButton.h>

#import <FontAwesomeKit/FAKFontAwesome.h>
#import "FAKIonIcons.h"
#import "FAKFoundationIcons.h"
#import "FAKZocial.h"

#import "Utilities.h"

#import "Scheduler.h"

#import "PietaTabs.h"

@interface MySchedule : UITableViewController <MGSwipeTableCellDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) NSArray *eventIdentifiers;
@property (strong, nonatomic) NSArray *schedules;
@property (strong, nonatomic) EKEvent *eventToDelete;

@end
