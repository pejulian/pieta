//
//  PrayerDetail.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/4/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ObjectiveSugar/ObjectiveSugar.h>

#import <FontAwesomeKit/FAKFontAwesome.h>
#import "FAKIonIcons.h"
#import "FAKFoundationIcons.h"
#import "FAKZocial.h"

#import <MZFormSheetController/MZFormSheetSegue.h>
#import <MZAppearance/MZAppearance.h>
#import <MZFormSheetController/MZFormSheetController.h>

#import <TWMessageBarManager/TWMessageBarManager.h>

#import "Utilities.h"

#import "Chapter.h"
#import "Content.h"
#import "Element.h"

#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>

#import <MessageUI/MessageUI.h> 

#import "PrayerOptions.h"

#import "PietaTabs.h"

@interface PrayerDetail : UIViewController <UITextViewDelegate, MFMailComposeViewControllerDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) Chapter *chapter;
@property (strong, nonatomic) NSArray<Content> *contents;
@property (assign, nonatomic) CGSize keyboardSize;
@property (strong, nonatomic) UITextView* textView;

@end
