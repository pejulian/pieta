//
//  Scheduler.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/12/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TWMessageBarManager/TWMessageBarManager.h>

#import <FontAwesomeKit/FAKFontAwesome.h>
#import "FAKIonIcons.h"

#import "Utilities.h"
#import "Book.h"
#import "Chapter.h"

#import "PietaTabs.h"

#import "MSCellAccessory.h"

@interface Scheduler : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) Chapter *chapter;
@property (strong, nonatomic) EKEvent *event;

@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;
@property (strong, nonatomic) NSDate *startTime;
@property (strong, nonatomic) NSDate *endTime;

@property (assign, nonatomic) NSInteger reminderTime;
@property (strong, nonatomic) NSMutableArray *reminderTimes;
@property (strong, nonatomic) NSMutableArray *repeatDays;

@property (strong, nonatomic) UIDatePicker *startDatePicker;
@property (strong, nonatomic) UIDatePicker *endDatePicker;
@property (strong, nonatomic) UIDatePicker *startTimePicker;
@property (strong, nonatomic) UIDatePicker *endTimePicker;
@property (strong, nonatomic) UIPickerView *scheduleReminderPicker;

@property (weak, nonatomic) IBOutlet UITextField *scheduleStartDate;
@property (weak, nonatomic) IBOutlet UITextField *scheduleEndDate;

@property (weak, nonatomic) IBOutlet UITextField *scheduleStartTime;
@property (weak, nonatomic) IBOutlet UITextField *scheduleEndTime;

@property (weak, nonatomic) IBOutlet UITextField *scheduleReminder;
@property (weak, nonatomic) IBOutlet UITableView *scheduleDays;

- (void) setSchedule:(Chapter *)chapter event:(EKEvent *)event;

@end
