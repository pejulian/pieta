//
//  Information.m
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 1/19/15.
//  Copyright (c) 2015 Julian Pereira. All rights reserved.
//

#import "Information.h"

@interface Information ()

@end

@implementation Information

- (void) awakeFromNib {
    
    CGFloat iconSize = [Utilities tabBarIconSize];
    FAKIcon *iconSelected = [FAKIonIcons iosInformationIconWithSize:iconSize];
    FAKIcon *icon = [FAKIonIcons iosInformationOutlineIconWithSize:iconSize];
    
    [iconSelected setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [icon setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    CGSize iconDim = [Utilities tabBarIconDimension];
    self.tabBarItem.image = [[icon imageWithSize:iconDim] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.tabBarItem.selectedImage = [[iconSelected imageWithSize:iconDim] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    
    self.tabBarItem.title = nil;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];

    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width - 150, self.navigationController.navigationBar.frame.size.height)];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [titleLabel setNumberOfLines:0];
    [titleLabel setTextColor:[UIColor whiteColor]];
    
    NSDictionary *titleAttr = @{ NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:[UIFont labelFontSize]] };
    
    NSAttributedString* attributedTitle = [[NSAttributedString alloc] initWithString:@"App Information" attributes:titleAttr];
    [titleLabel setAttributedText:attributedTitle];
    self.navigationItem.titleView = titleLabel;
    
    self.book = [Utilities book];
    self.chapter = [[[self.book chapters] select:^BOOL(Chapter *c){
        return [c informativeText];
    }] firstObject];
    
    [self createTextView];
}

- (void)createTextView {
    
    NSTextStorage *textStorage = [Utilities buildTextForChapter:self.chapter];
    
    // =======================================
    // Text View Setup
    // =======================================
    
    CGRect newTextViewRect = self.view.bounds;
    
    // 1. Create the layout manager
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    
    // 2. Create a text container
    CGSize containerSize = CGSizeMake(newTextViewRect.size.width, newTextViewRect.size.height);
    
    NSTextContainer *container = [[NSTextContainer alloc] initWithSize:containerSize];
    container.widthTracksTextView = NO;
    container.heightTracksTextView = NO;
    
    [layoutManager addTextContainer:container];
    [textStorage addLayoutManager:layoutManager];
    
    // 4. Create a UITextView
    _textView = [[UITextView alloc] initWithFrame:
                 CGRectMake(newTextViewRect.origin.x, newTextViewRect.origin.y,  newTextViewRect.size.width, newTextViewRect.size.height - 30) textContainer:container];
    
    _textView.editable = NO;
    _textView.selectable = YES;
    _textView.delegate = self;
    _textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    // 6. Add text view to main view
    [self.view addSubview:_textView];
    
    // Ugly hack to simulate a scroll (this solves scrolling issue with search)
    _textView.contentOffset = CGPointMake(0, 1);
    _textView.contentOffset = CGPointMake(0, 0);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
