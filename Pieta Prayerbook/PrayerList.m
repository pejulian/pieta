//
//  PrayerList.m
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/4/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "PrayerList.h"

@interface PrayerList ()

@end

@implementation PrayerList


- (void) awakeFromNib {
    
    CGFloat iconSize = [Utilities tabBarIconSize];
    FAKIcon *iconSelected = [FAKIonIcons iosBookmarksIconWithSize:iconSize];
    FAKIcon *icon = [FAKIonIcons iosBookmarksOutlineIconWithSize:iconSize];
    
    [iconSelected setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [icon setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    CGSize iconDim = [Utilities tabBarIconDimension];
    self.tabBarItem.image = [[icon imageWithSize:iconDim] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.tabBarItem.selectedImage = [[iconSelected imageWithSize:iconDim] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);

    self.tabBarItem.title = nil;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[UISearchBar appearance] setBarTintColor:[[Utilities colors] objectForKey:@"tab_bar_bg"]];
    
    self.searchDisplayController.searchResultsTableView.separatorInset = UIEdgeInsetsZero;

    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width - 150, self.navigationController.navigationBar.frame.size.height)];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
    [titleLabel setNumberOfLines:0];
    [titleLabel setTextColor:[UIColor whiteColor]];
    
    NSDictionary *titleAttr = @{ NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:[UIFont labelFontSize]] };

    NSAttributedString* attributedTitle = [[NSAttributedString alloc] initWithString:@"Pieta Prayerbook" attributes:titleAttr];
    [titleLabel setAttributedText:attributedTitle];
    self.navigationItem.titleView = titleLabel;
    
    // http://stackoverflow.com/questions/19417179/multi-line-uinavigationbar
    dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 1.0);
    dispatch_after(delay, dispatch_get_main_queue(), ^(void) {
        [self.navigationItem setPrompt:@"Swipe left on a prayer to view options"];
    });
    
    delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 5.0);
    dispatch_after(delay, dispatch_get_main_queue(), ^(void) {
        [self.navigationItem setPrompt:nil];
    });
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.book = [Utilities book];
    
    self.chapters = (NSArray<Chapter>*)[[self.book chapters] reject:^BOOL(Chapter *c) {
        return [c informativeText];
    }];
    
    [self.tableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(preferredContentSizeChanged:) name:UIContentSizeCategoryDidChangeNotification object:nil];
    
}

- (void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    self.book = nil;
    self.chapters = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIContentSizeCategoryDidChangeNotification object:nil];
}

- (void) dealloc {
    
    self.book = nil;
    self.chapters = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)preferredContentSizeChanged:(NSNotification *)notification {
    [self.tableView reloadData];
}

#pragma mark - Table view data source


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static UILabel* label;
    
    if (!label) {
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, FLT_MAX, FLT_MAX)];
        label.text = @"dummy text";
    }
    
    [label setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleBody]];
    [label setNumberOfLines:0];
    [label sizeToFit];
    
    return label.frame.size.height * 3.4;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ( tableView == self.searchDisplayController.searchResultsTableView )
        return [self.searchResults count];
    else
        return [self.chapters count];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ( tableView == self.searchDisplayController.searchResultsTableView ) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        // Chapter *chapter = (Chapter *)[self.searchResults objectAtIndex:indexPath.row];
        // [self performSegueWithIdentifier:@"prayerDetailSegue" sender:chapter];
    }
}

- (MGSwipeTableCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * reuseIdentifier = @"Cell";
    MGSwipeTableCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if ( cell == nil )
        cell = [[MGSwipeTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    // Display recipe in the table cell
    Chapter *chapter = nil;
    if ( tableView == self.searchDisplayController.searchResultsTableView )
        chapter = [self.searchResults objectAtIndex:indexPath.row];
    else
        chapter = [self.chapters objectAtIndex:indexPath.row];
    
    // Cell icon
    FAKIcon *icon = [FAKIonIcons iosBookmarksOutlineIconWithSize:28];
    [icon setAttributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    [cell.imageView setImage:[icon imageWithSize:CGSizeMake(28, 28)]];
    
    NSDictionary *titleAttr = @{ NSFontAttributeName:[UIFont fontWithName:@"Palatino-Italic" size:[UIFont labelFontSize]] };
    NSAttributedString* attributedTitle = [[NSAttributedString alloc] initWithString:[chapter title] attributes:titleAttr];
    [cell.textLabel setAttributedText:attributedTitle];
    
    FAKIcon *scheduleIcon = [FAKIonIcons iosStopwatchOutlineIconWithSize:42];
    [scheduleIcon setAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
    
    FAKIcon *favouriteIcon = [FAKIonIcons iosStarOutlineIconWithSize:42];
    if ( [Utilities isChapterInBookmarks:[chapter id]] ) {
        favouriteIcon = [FAKIonIcons iosStarIconWithSize:42];
        [favouriteIcon setAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
    }
    
    MGSwipeButton *favourite = [MGSwipeButton buttonWithTitle:@"" icon:[favouriteIcon imageWithSize:CGSizeMake(48,48)] backgroundColor:[UIColor greenColor]];
    MGSwipeButton *schedule = [MGSwipeButton buttonWithTitle:@"" icon:[scheduleIcon imageWithSize:CGSizeMake(48,48)] backgroundColor:[UIColor cyanColor]];

    // configure right buttons
    cell.delegate = self;
    cell.rightButtons = @[favourite, schedule];
    cell.rightSwipeSettings.transition = MGSwipeTransitionDrag;

    return cell;
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion {
    
    Chapter *chapter = nil;
    NSIndexPath *indexPath = nil;

    if ( self.searchDisplayController.active ) {
        indexPath = [self.searchDisplayController.searchResultsTableView indexPathForCell:cell];
        chapter = [self.searchResults objectAtIndex:[indexPath row]];
    }
    else {
        indexPath = [self.tableView indexPathForCell:cell];
        chapter = [self.chapters objectAtIndex:[indexPath row]];
    }
    
    switch( direction ) {
        case MGSwipeDirectionRightToLeft:
            switch( index ) {
                case 0:
                    
                    if ( [Utilities isChapterInBookmarks:[chapter id]] ) {
                        
                        if ( [Utilities removeChapterFromBookmarks:[chapter id]] ) {
                            
                            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Success" description:@"Prayer has been removed from bookmarks" type:TWMessageBarMessageTypeSuccess duration:2.0];
                        }
                    }
                    else {
                        
                        if ( [Utilities addChapterToBookmarks:[chapter id]] ) {
                            
                            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Success" description:@"Prayer add to bookmarks" type:TWMessageBarMessageTypeSuccess duration:2.0];
                        }
                    }
                    
                    [self.tableView reloadData];
                    
                    break;
                case 1:
                    [self performSegueWithIdentifier:@"schedulerSegue" sender:chapter];
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
 
    return YES;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    
    NSPredicate *resultPredicate = [NSPredicate predicateWithBlock:^BOOL(Chapter *c, NSDictionary *bindings) {
        return [c.title containsString:searchText];
    }];
    self.searchResults = (NSArray<Chapter>*)[self.chapters filteredArrayUsingPredicate:resultPredicate];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    return YES;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Set prompt to nil in case its still being displayed at time of view change
    [self.navigationItem setPrompt:nil];

    
    NSIndexPath *indexPath = nil;
    Chapter *chapter = nil;
    
    if ( [[segue identifier] isEqualToString:@"prayerDetailSegue"] ) {
        
        if ( self.searchDisplayController.active ) {
            indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
            chapter = [self.searchResults objectAtIndex:indexPath.row];
        } else {
            indexPath = [self.tableView indexPathForSelectedRow];
            chapter = [self.chapters objectAtIndex:indexPath.row];
        }
        
        [[segue destinationViewController] setChapter:chapter];
    }
    else if ( [[segue identifier] isEqualToString:@"schedulerSegue"] ) {
        
        chapter = (Chapter *)sender;
        [[segue destinationViewController] setChapter:chapter];
        
    }
    else {}
    
    
}

@end
