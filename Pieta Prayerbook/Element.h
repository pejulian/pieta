//
//  Element.h
//  Pieta Prayerbook
//
//  Created by Julian Matthew Nunis Pereira on 11/3/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol Element @end

@interface Element : JSONModel

@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSString* type;
@property (strong, nonatomic) NSArray* items;

@end
